﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Tonakai.Models.API.Section;
using Tonakai.Services;

namespace Tonakai.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SectionController : Controller
    {
        private readonly SectionDataService _sections;

        public SectionController(SectionDataService sections)
        {
            _sections = sections;
        }

        [HttpGet("Project/{projectId}")]
        public async Task<ActionResult<SectionInfo[]>> ProjectGet(int projectId, bool full = false)
        {
            return await _sections.GetSectionsAsync(projectId, full);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<SectionInfo>> Get(int id)
        {
            try
            {
                return Ok(await _sections.GetSectionAsync(id));
            }
            catch (DataServiceException e)
            {
                return BadRequest(e.ApiError);
            }
        }

        [HttpPost]
        public async Task<ActionResult<SectionInfo>> Post(SectionCreate section)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            try
            {
                return Ok(await _sections.CreateSectionAsync(section));
            }
            catch (DataServiceException e)
            {
                return BadRequest(e.ApiError);
            }
        }

        [HttpPut]
        public async Task<ActionResult<SectionInfo>> Put(SectionInfo section)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            try
            {
                return Ok(await _sections.ChangeSectionAsync(section));
            }
            catch (DataServiceException e)
            {
                return BadRequest(e.ApiError);
            }
        }
    }
}
