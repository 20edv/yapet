﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Tonakai.Models.API.Equipment;
using Tonakai.Services;

namespace Tonakai.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EquipmentController : Controller
    {
        private readonly EquipmentDataService _equip;

        public EquipmentController(EquipmentDataService equip)
        {
            _equip = equip;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<EquipmentInfo[]>> Get(int id)
        {
            try
            {
                return Ok(await _equip.GetEquipmentAsync(id));
            }
            catch (DataServiceException e)
            {
                return BadRequest(e.ApiError);
            }
        }

        [HttpPost]
        public async Task<ActionResult<EquipmentInfo>> Post(EquipmentCreate equipment)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            try
            {
                return Ok(await _equip.CreateEquipmentAsync(equipment));
            }
            catch (DataServiceException e)
            {
                return BadRequest(e.ApiError);
            }
        }

        [HttpPut]
        public async Task<ActionResult<EquipmentInfo>> Put(EquipmentInfo section)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            try
            {
                return Ok(await _equip.ChangeEquipmentAsync(section));
            }
            catch (DataServiceException e)
            {
                return BadRequest(e.ApiError);
            }
        }
    }
}
