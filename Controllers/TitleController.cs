﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Tonakai.Models.API.Titles;
using Tonakai.Services;

namespace Tonakai.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TitleController : Controller
    {
        private readonly TitleDataService _titles;

        public TitleController(TitleDataService titles)
        {
            _titles = titles;
        }

        [HttpGet("Project/{projectId}")]
        public async Task<ActionResult<TitleInfo[]>> ProjectGet(int projectId)
        {
            return await _titles.GetTitlesAsync(projectId);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TitleInfo>> Get(int id)
        {
            try
            {
                return await _titles.GetTitleAsync(id);
            }
            catch (DataServiceException e)
            {
                return BadRequest(e.ApiError);
            }
        }

        [HttpPost]
        public async Task<ActionResult<TitleInfo>> Post(TitleCreate title)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            try
            {
                return await _titles.CreateTitleAsync(title);
            }
            catch (DataServiceException e)
            {
                return BadRequest(e.ApiError);
            }
        }

        [HttpPut]
        public async Task<ActionResult<TitleInfo>> Put(TitleInfo title)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            try
            {
                return await _titles.ChangeTitleAsync(title);
            }
            catch (DataServiceException e)
            {
                return BadRequest(e.ApiError);
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _titles.DeleteTitleAsync(id);
                return Ok();
            }
            catch (DataServiceException e)
            {
                return BadRequest(e.ApiError);
            }
        }

        [HttpPost("Recover/{id}")]
        public async Task<ActionResult> Recover(int id)
        {
            try
            {
                await _titles.RecoverTitleAsync(id);
                return Ok();
            }
            catch (DataServiceException e)
            {
                return BadRequest(e.ApiError);
            }
        }
    }
}
