﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Tonakai.Models.API.Equipment;
using Tonakai.Models.API.Project;
using Tonakai.Models.API.Section;
using Tonakai.Services;

namespace Tonakai.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectController : Controller
    {
        private readonly ProjectDataService _projects;

        public ProjectController(ProjectDataService projects)
        {
            _projects = projects;
        }

        [HttpGet]
        public async Task<ActionResult<ProjectInfo[]>> Get()
        {
            return await _projects.GetProjectsAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ProjectInfo>> Get(int id)
        {
            try
            {
                return Ok(await _projects.GetProjectAsync(id));
            }
            catch (DataServiceException e)
            {
                return BadRequest(e.ApiError);
            }
        }

        [HttpPost]
        public async Task<ActionResult<ProjectInfo>> Post(ProjectCreate project)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            try
            {
                return Ok(await _projects.CreateProjectAsync(project));
            }
            catch (DataServiceException e)
            {
                return BadRequest(e.ApiError);
            }
        }

        [HttpPut]
        public async Task<ActionResult<ProjectInfo>> Put(ProjectInfo project)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            try
            {
                return Ok(await _projects.ChangeProjectAsync(project));
            }
            catch (DataServiceException e)
            {
                return BadRequest(e.ApiError);
            }
        }

        [HttpGet("{projectId}/Stats")]
        public async Task<ActionResult<ProjectStats>> GetStats(int projectId)
        {
            try
            {
                return Ok(await _projects.GetProjectStatsAsync(projectId));
            }
            catch (DataServiceException e)
            {
                return BadRequest(e.ApiError);
            }
        }
    }
}
