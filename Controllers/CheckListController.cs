﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Tonakai.Contexts;
using Tonakai.Models.API;
using Tonakai.Models.API.CheckList;
using Tonakai.Services;

namespace Tonakai.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CheckListController : Controller
    {
        private readonly CheckListDataService _checklists;

        public CheckListController(CheckListDataService checklists)
        {
            _checklists = checklists;
        }

        [HttpGet("Project/{projectId}")]
        public async Task<ActionResult<CheckListInfo[]>> ProjectGet(int projectId, bool full = false)
        {
            return await _checklists.GetCheckListsAsync(projectId, full);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<CheckListInfo>> Get(int id)
        {
            try
            {
                return await _checklists.GetCheckListAsync(id);
            }
            catch (DataServiceException e)
            {
                return BadRequest(e.ApiError);
            }
        }

        [HttpPost]
        public async Task<ActionResult<CheckListInfo>> Post(CheckListCreate checklist)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            try
            {
                return await _checklists.CreateCheckListAsync(checklist);
            }
            catch (DataServiceException e)
            {
                return BadRequest(e.ApiError);
            }
        }

        [HttpPut]
        public async Task<ActionResult<CheckListInfo>> Put(CheckListInfo checklist)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            try
            {
                return await _checklists.ChangeCheckListAsync(checklist);
            }
            catch (DataServiceException e)
            {
                return BadRequest(e.ApiError);
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _checklists.DeleteCheckListAsync(id);
                return Ok();
            }
            catch (DataServiceException e)
            {
                return BadRequest(e.ApiError);
            }
        }

        [HttpPost("Item")]
        public async Task<ActionResult<CheckListItemInfo>> PostItem(CheckListItemCreate clitem)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            try
            {
                return await _checklists.CreateCheckListItemAsync(clitem);
            }
            catch (DataServiceException e)
            {
                return BadRequest(e.ApiError);
            }
        }

        [HttpPut("Item")]
        public async Task<ActionResult<CheckListItemInfo>> PutItem(CheckListItemInfo clitem)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            try
            {
                return await _checklists.ChangeCheckListItemAsync(clitem);
            }
            catch (DataServiceException e)
            {
                return BadRequest(e.ApiError);
            }
        }

        [HttpDelete("Item/{id}")]
        public async Task<ActionResult> DeleteItem(int id)
        {
            try
            {
                await _checklists.DeleteCheckListItemAsync(id);
                return Ok();
            }
            catch (DataServiceException e)
            {
                return BadRequest(e.ApiError);
            }
        }
    }
}
