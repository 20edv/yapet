using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Tonakai.Contexts;
using Tonakai.Models.API;
using Tonakai.Models.API.Notes;
using Tonakai.Services;
using static Tonakai.Models.DB.CheckNote;

namespace Tonakai.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NoteController : Controller
    {
        private readonly NoteDataService _notes;

        public NoteController(NoteDataService notes)
        {
            _notes = notes;
        }

        [HttpGet("Project/{projectId}")]
        public async Task<ActionResult<NoteInfo[]>> GetNotes(int projectId, bool data = false, bool events = false, bool deleted = false)
        {
            return await _notes.GetNotesAsync(projectId, data, events, deleted);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<NoteInfo>> GetNote(int id, bool data = true, bool events = true)
        {
            try
            {
                return await _notes.GetNoteAsync(id, data, events);
            }
            catch (DataServiceException e)
            {
                return NotFound(e.ApiError);
            }
        }

        [HttpPost]
        public async Task<ActionResult<NoteInfo>> Post([FromBody] NoteCreate note)
        {
            if (User == null && User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier) != null)
                return Unauthorized();

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                return await _notes.CreateNoteAsync(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value, note);
            }
            catch (DataServiceException e)
            {
                return BadRequest(e.ApiError);
            }
        }

        [HttpPut]
        public async Task<ActionResult<NoteInfo>> Update([FromBody] NoteInfo note)
        {
            if (User == null && User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier) != null)
                return Unauthorized();

            try
            {
                return await _notes.UpdateNoteAsync(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value, note);
            }
            catch (DataServiceException e)
            {
                return BadRequest(e.ApiError);
            }
        }

        [HttpDelete]
        public async Task<ActionResult> Delete(int id, StatusEnum status)
        {
            if (User == null && User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier) != null)
                return Unauthorized();

            try
            {
                await _notes.ChangeNoteStatusAsync(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value, id, StatusEnum.Deleted);
                return Ok();
            }
            catch (DataServiceException e)
            {
                return BadRequest(e.ApiError);
            }
        }

        [HttpPost("Comment")]
        public async Task<ActionResult<NoteEventInfo>> AddComment([FromBody] NoteCommentCreate comment)
        {
            if (User == null && User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier) != null)
                return Unauthorized();

            try
            {
                return await _notes.AddCommentAsync(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value, comment);
            }
            catch (DataServiceException e)
            {
                return BadRequest(e.ApiError);
            }
        }

        [HttpPost("Answer")]
        public async Task<ActionResult<NoteEventInfo>> AddAnswer([FromBody] NoteCommentCreate comment)
        {
            if (User == null && User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier) != null)
                return Unauthorized();

            try
            {
                return await _notes.AddCommentAsync(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value, comment, true);
            }
            catch (DataServiceException e)
            {
                return BadRequest(e.ApiError);
            }
        }

        [HttpPost("{id}/Status/{status}")]
        public async Task<ActionResult<NoteEventInfo>> ChangeStatus(int id, StatusEnum status)
        {
            if (User == null && User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier) != null)
                return Unauthorized();

            try
            {
                return await _notes.ChangeNoteStatusAsync(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value, id, status);
            }
            catch (DataServiceException e)
            {
                return BadRequest(e.ApiError);
            }
        }

        [HttpPost("{id}/Iteration/{iteration}")]
        public async Task<ActionResult<NoteEventInfo>> ChangeIteration(int id, uint iteration)
        {
            if (User == null && User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier) != null)
                return Unauthorized();

            try
            {
                return await _notes.ChangeIteration(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value, id, iteration);
            }
            catch (DataServiceException e)
            {
                return BadRequest(e.ApiError);
            }
        }

        ////private async Task<string> ProcessFile(NoteFull.File file)
        ////{
        ////    if (file.Id != 0)
        ////        return file.Id;

        ////    var img = file.Data.Split(";");
        ////    var f = new Models.DB.File
        ////    {
        ////        Name = file.Name,
        ////        Data = Convert.FromBase64String(img[1].Substring(7)),
        ////        MimeType = img[0].Substring(5),
        ////    };
        ////    f.Size = f.Data.Length;
        ////    _context.Files.Add(f);
        ////    await _context.SaveChangesAsync();
        ////    return f.Id;
        ////}

        //[HttpPost("Comment")]
        //[RequestSizeLimit(100_000_000)]
        //public async Task<ActionResult> PostComment([FromBody] NoteComment comment)
        //{
        //    //if (User == null && User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier) != null)
        //    //    return Unauthorized();

        //    //var ev = new Models.DB.CheckNoteEvent
        //    //{
        //    //    NoteId = comment.NoteId,
        //    //    Files = new int[0],
        //    //    Type = Models.DB.CheckNoteEvent.EventType.Comment,
        //    //    Time = DateTime.Now,
        //    //    UserId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value,
        //    //    Data = System.Text.Json.JsonDocument.Parse(System.Text.Json.JsonSerializer.Serialize(new Models.DB.CheckNoteEvent.DataTypes.Comment { Text = comment.Text }))
        //    //};

        //    //await _context.CheckNoteEvents.AddAsync(ev);
        //    //await _context.SaveChangesAsync();

        //    //if (comment.Files.Length > 0)
        //    //{
        //    //    var fileIds = new List<int>();

        //    //    foreach (var file in comment.Files)
        //    //        fileIds.Add(await ProcessFile(file));

        //    //    ev.Files = fileIds.ToArray();
        //    //    await _context.SaveChangesAsync();
        //    //}

        //    //var ret = new NoteFull.NoteEvent
        //    //{
        //    //    Id = ev.Id,
        //    //    Data = ev.Data.RootElement,
        //    //    Files = _context.Files.Where(f => ev.Files.Contains(f.Id)).Select(f => new NoteFull.File
        //    //    {
        //    //        Id = f.Id,
        //    //        Mime = f.MimeType,
        //    //        Name = f.Name,
        //    //        Size = f.Size
        //    //    }).ToArray(),
        //    //    Time = ev.Time,
        //    //    Type = ev.Type,
        //    //    User = await _context.Users.Where(u => u.Id == ev.UserId).Select(u => u.Name).FirstOrDefaultAsync(),
        //    //    UserId = ev.UserId
        //    //};

        //    //return Json(ret);
        //    return null;
        //}

        //[HttpPut]
        //public async Task<ActionResult<NoteFull>> UpdateNote(NoteFull note)
        //{
        //    //if (User == null && User.Claims.Count() == 0)
        //    //    return Unauthorized();

        //    //var nt = await _context.CheckNotes.FirstOrDefaultAsync(u => u.Id == note.Id);
        //    //if (nt == null)
        //    //    return NotFound(ApiError.NotFound());

        //    //nt.Title = note.Title;
        //    //nt.Position = note.Position;
        //    //nt.Priority = note.Priority;
        //    //nt.Section = note.Section;
        //    //nt.Text = note.Text;

        //    //await _context.SaveChangesAsync();

        //    //if (nt.Files.Length != 0 || note.Files.Length != 0)
        //    //{
        //    //    var fileIds = new List<int>();

        //    //    foreach (var file in note.Files)
        //    //        fileIds.Add(await ProcessFile(file));

        //    //    nt.Files = fileIds.ToArray();
        //    //    await _context.SaveChangesAsync();
        //    //}

        //    //var ev = new Models.DB.CheckNoteEvent
        //    //{
        //    //    NoteId = nt.Id,
        //    //    Files = new int[0],
        //    //    Type = Models.DB.CheckNoteEvent.EventType.Edit,
        //    //    Time = DateTime.Now,
        //    //    UserId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value,
        //    //    Data = System.Text.Json.JsonDocument.Parse("{}")
        //    //};

        //    //await _context.CheckNoteEvents.AddAsync(ev);
        //    //await _context.SaveChangesAsync();

        //    //return await GetNote(note.Id);
        //    return null;
        //}

        //[HttpPost("{id}/Status/{status}")]
        //public async Task<ActionResult<NoteFull>> UpdateNote(int id, byte status)
        //{
        //    if (User == null && User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier) != null)
        //        return Unauthorized();

        //    var nt = await _context.CheckNotes.FirstOrDefaultAsync(u => u.Id == id);
        //    if (nt == null)
        //        return NotFound(ApiError.NotFound());

        //    var from = nt.Status;
        //    nt.Status = (StatusEnum)status;
        //    var to = nt.Status;

        //    await _context.SaveChangesAsync();

        //    var ev = new Models.DB.CheckNoteEvent
        //    {
        //        NoteId = nt.Id,
        //        //Files = new int[0],
        //        Type = Models.DB.CheckNoteEvent.EventType.StatusChange,
        //        Time = DateTime.Now,
        //        UserId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value,
        //        Data = System.Text.Json.JsonDocument.Parse(System.Text.Json.JsonSerializer.Serialize(new Models.DB.CheckNoteEvent.DataTypes.StatusChange { From = from, To = to }))
        //    };

        //    await _context.CheckNoteEvents.AddAsync(ev);
        //    await _context.SaveChangesAsync();

        //    var ret = new NoteFull.NoteEvent
        //    {
        //        Id = ev.Id,
        //        Data = ev.Data.RootElement,
        //        Files = new NoteFull.File[0],
        //        Time = ev.Time,
        //        Type = ev.Type,
        //        User = await _context.Users.Where(u => u.Id == ev.UserId).Select(u => u.FirstName).FirstOrDefaultAsync(),
        //        UserId = ev.UserId
        //    };

        //    return Ok(ret);
        //}

        //[HttpDelete("{id}")]
        //public async Task<ActionResult> DeleteNote(int id)
        //{
        //    var note = await _context.CheckNotes.FirstOrDefaultAsync(t => t.Id == id);
        //    if (note == null)
        //        return NotFound(ApiError.NotFound());
        //    //_context.CheckNotes.Remove(note);
        //    note.Status = StatusEnum.Deleted;

        //    var ev = new Models.DB.CheckNoteEvent
        //    {
        //        NoteId = id,
        //        //Files = new int[0],
        //        Type = Models.DB.CheckNoteEvent.EventType.Delete,
        //        Time = DateTime.Now,
        //        UserId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value,
        //        Data = System.Text.Json.JsonDocument.Parse("{}")
        //    };

        //    await _context.CheckNoteEvents.AddAsync(ev);
        //    await _context.SaveChangesAsync();

        //    var ret = new NoteFull.NoteEvent
        //    {
        //        Id = ev.Id,
        //        Data = ev.Data.RootElement,
        //        Files = new NoteFull.File[0],
        //        Time = ev.Time,
        //        Type = ev.Type,
        //        User = await _context.Users.Where(u => u.Id == ev.UserId).Select(u => u.FirstName).FirstOrDefaultAsync(),
        //        UserId = ev.UserId
        //    };

        //    await _context.SaveChangesAsync();
        //    return Ok();
        //}
    }
}
