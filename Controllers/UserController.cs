﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Tonakai.Contexts;
using Tonakai.Models.API;
using Tonakai.Models.API.User;
using Tonakai.Services;

namespace Tonakai.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : Controller
    {
        private readonly UserDataService _users;

        public UserController(UserDataService users)
        {
            _users = users;
        }

        [HttpGet]
        public async Task<ActionResult<UserInfo[]>> Get()
        {
            return await _users.GetUsersAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<UserInfo>> Get(string id)
        {
            try
            {
                return Ok(await _users.GetUserAsync(id));
            }
            catch (DataServiceException e)
            {
                return BadRequest(e.ApiError);
            }
        }

        [HttpPost]
        public async Task<ActionResult<UserInfo>> Post(UserCreate user)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            try
            {
                return Ok(await _users.CreateUserAsync(user));
            }
            catch (DataServiceException e)
            {
                return BadRequest(e.ApiError);
            }
        }

        [HttpPut]
        public async Task<ActionResult<UserInfo>> Put(UserInfo user)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            try
            {
                return Ok(await _users.ChangeUserAsync(user));
            }
            catch (DataServiceException e)
            {
                return BadRequest(e.ApiError);
            }
        }
    }
}
