﻿using ClosedXML.Report;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tonakai.Models.Report;
using Tonakai.Services;

namespace Tonakai.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportController : Controller
    {
        const string ProjectNotesTemplate = "./Templates/NotesReport.xlsx";

        private readonly ProjectDataService _projects;
        private readonly NoteDataService _notes;
        private readonly FileDataService _files;

        public ReportController(ProjectDataService projects, NoteDataService notes, FileDataService files)
        {
            _projects = projects;
            _notes = notes;
            _files = files;
        }

        [HttpGet("Project/{id}")]
        public async Task<ActionResult> Project(int id)
        {
            try
            {
                var project = await _projects.GetProjectAsync(id);
                var notes = (await _notes.GetNotesAsync(id, true, false, false)).Select(n => new CheckNoteInfo { Note = n }).ToArray();
                var date = DateTime.Now;

                var template = new XLTemplate(ProjectNotesTemplate);
                template.AddVariable("ProjectName", project.Name);
                template.AddVariable("ReportTime", date.ToString());
                template.AddVariable("Notes", notes);
                template.Generate();

                MemoryStream ms = new MemoryStream();
                template.SaveAs(ms);
                ms.Seek(0, SeekOrigin.Begin);
                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", $"Замечания {project.Name} {date}.xlsx");
            }
            catch (DataServiceException e)
            {
                return BadRequest(e.ApiError);
            }
        }

        [HttpGet("Notes/{id}")]
        public async Task<ActionResult> Notes(int id, ReportFormat format = 0)
        {
            try
            {
                var notes = await _notes.GetNotesAsync(id, true, false, false);
                var project = await _projects.GetProjectAsync(id);

                var files = new Dictionary<string, string>();
                if (format != ReportFormat.HTML)
                {
                    foreach (var note in notes)
                        foreach (var file in note.Files)
                            if (file.MimeType.StartsWith("image") && !files.ContainsKey(file.Id))
                            {
                                var fn = "./Temp/" + Guid.NewGuid().ToString() + "." + file.MimeType.Split('/')[1];
                                var (f, i) = await _files.GetFile(file.Id);
                                System.IO.File.WriteAllBytes(fn, f);
                                files.Add(i.Id, fn);
                            }
                }

                var html = $"<!DOCTYPE html><html lang=\"ru\"><head><meta charset=\"UTF-8\"><title>Замечания {project.Name}</title></head><body>";
                foreach (var note in notes)
                {
                    html += await GenNote(note, format == 0 ? null : files);
                    if (format == ReportFormat.Word)
                        html += "\n<div style=\"page-break-after: always;\"></div>";
                }
                html += $"</body></html>";

                if (format == ReportFormat.HTML)
                    return File(Encoding.UTF8.GetBytes(html), "text/html", $"Замечания {project.Name}.html");
                else
                {
                    byte[] file;
                    if (format == ReportFormat.Word)
                        file = await GenWordFile(html);
                    else
                        file = await GenPdfFile(html);
                    foreach (var f in files)
                        System.IO.File.Delete(f.Value);
                    return File(file, format == ReportFormat.Word ? "application/vnd.openxmlformats-officedocument.wordprocessingml.document" : "application/pdf", $"Замечания {project.Name}." + (format == ReportFormat.Word ? "docx" : "pdf") );
                }

            }
            catch (DataServiceException e)
            {
                return BadRequest(e.ApiError);
            }
        }

        [HttpGet("Note/{id}")]
        public async Task<ActionResult> Note(int id, ReportFormat format = 0)
        {
            try
            {
                var note = await _notes.GetNoteAsync(id);

                var files = new Dictionary<string, string>();
                if (format != ReportFormat.HTML)
                {
                    foreach (var file in note.Files)
                        if (file.MimeType.StartsWith("image") && !files.ContainsKey(file.Id))
                        {
                            var fn = "./Temp/" + Guid.NewGuid().ToString() + "." + file.MimeType.Split('/')[1];
                            var (f, i) = await _files.GetFile(file.Id);
                            System.IO.File.WriteAllBytes(fn, f);
                            files.Add(i.Id, fn);
                        }
                }

                var html = $"<!DOCTYPE html><html lang=\"ru\"><head><meta charset=\"UTF-8\"><title>Замечание #{note.Id}</title></head><body>";
                html += await GenNote(note, format == 0 ? null : files);
                html += $"</body></html>";

                if (format == ReportFormat.HTML)
                    return File(Encoding.UTF8.GetBytes(html), "text/html", $"Замечание {note.Id}.html");
                else
                {
                    byte[] file;
                    if (format == ReportFormat.Word)
                        file = await GenWordFile(html);
                    else
                        file = await GenPdfFile(html);
                    foreach (var f in files)
                        System.IO.File.Delete(f.Value);
                    return File(file, format == ReportFormat.Word ? "application/vnd.openxmlformats-officedocument.wordprocessingml.document" : "application/pdf", $"Замечание {note.Id}." + (format == ReportFormat.Word ? "docx" : "pdf"));
                }

            }
            catch (DataServiceException e)
            {
                return BadRequest(e.ApiError);
            }
        }

        private async Task<string> ExecPandoc(string text, string args)
        {
            Process pandoc = new Process();
            ProcessStartInfo startInfo = new ProcessStartInfo("pandoc", args);
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.RedirectStandardError = true;
            startInfo.RedirectStandardInput = true;
            startInfo.RedirectStandardOutput = true;
            startInfo.UseShellExecute = false;
            startInfo.CreateNoWindow = true;
            pandoc.StartInfo = startInfo;
            pandoc.Start();

            foreach (var line in text.Split('\n'))
                await pandoc.StandardInput.WriteLineAsync(line);
            pandoc.StandardInput.Close();
            var res = await pandoc.StandardOutput.ReadToEndAsync();
            return res;
        }

        private async Task ExecPandoc(string args)
        {
            Process pandoc = new Process();
            ProcessStartInfo startInfo = new ProcessStartInfo("pandoc", args);
            pandoc.StartInfo = startInfo;
            pandoc.Start();
            pandoc.WaitForExit();
        }

        private async Task<string> GenNote(Models.API.Notes.NoteInfo note, Dictionary<string, string> files)
        {
            string html = $"<h1>Замечание #{note.Id}</h1><table style=\"width: 100%\"><tr><td><p><b>Статус: </b>{StatusToCode(note.Status)}</p><p><b>Титул: </b>{note.Title.Name}</p><p><b>Разделы: </b>{string.Join(", ", note.Sections.Select(e => e.Name))}</p><p><b>Позиции: </b>{string.Join(", ", note.Equipment.Select(e => e.Name))}</p></td>";
            
            html += $"<td width=\"350px\"><p><b>Создал: </b>{note.CreatedBy.FirstName} {note.CreatedBy.LastName}</p>";
            if (note.AnsweredBy != null)
                html += $"<p><b>Ответил: </b>{note.AnsweredBy.FirstName} {note.AnsweredBy.LastName}</p>";
            html += $"<p><b>Дата создания: </b>{note.CreateDate.ToString()}</p><p><b>Дата последнего обновления: </b>{note.UpdateDate.ToString()}</p></td></tr></table>";
            html += $"<p>{note.ShortText}</p><hr>";
            html += $"{await GenHtml(note.Text)}<hr>";

            foreach (var file in note.Files)
            {
                if (file.MimeType.StartsWith("image"))
                {
                    if (files == null)
                    {
                        var (f, i) = await _files.GetFile(file.Id);
                        html += $"<div style=\"margin-top: 5px\"><img style=\"width: 100%\" src=\"data:{i.MimeType};base64,{Convert.ToBase64String(f)}\"></div>";
                    }
                    else
                    {
                        html += $"<div style=\"margin-top: 5px\"><img style=\"width: 100%\" src=\"{files[file.Id]}\"></div>";
                    }
                }
            }

            return html;
        }

        private string StatusToCode(Models.DB.CheckNote.StatusEnum status)
        {
            switch (status)
            {
                case Models.DB.CheckNote.StatusEnum.Actual: return "<span style=\"color: red\">Актуально</span>";
                case Models.DB.CheckNote.StatusEnum.Closed: return "<span style=\"color: green\">Принято</span>";
                case Models.DB.CheckNote.StatusEnum.Deleted: return "Удалено";
                case Models.DB.CheckNote.StatusEnum.ReqCheck: return "<span style=\"color: orange\">Исправлено</span>";
                default: return "Неизв.";
            }
        }

        private async Task<string> GenHtml(string md)
        {
            var guid = Guid.NewGuid().ToString();
            System.IO.File.WriteAllText($"./Temp/{guid}.md", md);
            await ExecPandoc($"-f markdown_github -t html5 -i ./Temp/{guid}.md -o ./Temp/{guid}.html");
            var file = System.IO.File.ReadAllText($"./Temp/{guid}.html");
            System.IO.File.Delete($"./Temp/{guid}.md");
            System.IO.File.Delete($"./Temp/{guid}.html");
            return file;
        }

        private async Task<byte[]> GenWordFile(string html)
        {
            var guid = Guid.NewGuid().ToString();
            System.IO.File.WriteAllText($"./Temp/{guid}.html", html);
            await ExecPandoc($"-f html -t docx -i ./Temp/{guid}.html -o ./Temp/{guid}.docx --metadata title");
            var file = System.IO.File.ReadAllBytes($"./Temp/{guid}.docx");
            System.IO.File.Delete($"./Temp/{guid}.html");
            System.IO.File.Delete($"./Temp/{guid}.docx");
            return file;
        }

        private async Task<byte[]> GenPdfFile(string html)
        {
            var guid = Guid.NewGuid().ToString();
            System.IO.File.WriteAllText($"./Temp/{guid}.html", html);
            await ExecPandoc($"-f html -t pdf --pdf-engine wkhtmltopdf -i ./Temp/{guid}.html -o ./Temp/{guid}.pdf");
            var file = System.IO.File.ReadAllBytes($"./Temp/{guid}.pdf");
            System.IO.File.Delete($"./Temp/{guid}.html");
            System.IO.File.Delete($"./Temp/{guid}.pdf");
            return file;
        }

        public enum ReportFormat
        {
            HTML = 0,
            PDF = 1,
            Word = 2
        }
    }
}
