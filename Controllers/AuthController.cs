﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Tonakai.Models.DB;

namespace Tonakai.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : Controller
    {
        private readonly UserManager<User> _users;
        private readonly SignInManager<User> _signIn;

        public AuthController(UserManager<User> users, SignInManager<User> signIn)
        {
            _users = users;
            _signIn = signIn;
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] AuthModel auth)
        {
            var result = await _signIn.PasswordSignInAsync(auth.Login, auth.Password, false, false);

            if (result.Succeeded)
            {
                var appUser = _users.Users.FirstOrDefault(r => r.NormalizedUserName == auth.Login.ToUpper());
                return await GenerateJwtToken(appUser);
            }

            return BadRequest(new Models.API.ApiError("WRONG_PASSWORD"));
        }

        private async Task<JsonResult> GenerateJwtToken(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim(JwtRegisteredClaimNames.GivenName, $"{user.FirstName} {user.LastName}")
            };

            var key = AuthOptions.GetSymmetricSecurityKey();
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddHours(12);

            var token = new JwtSecurityToken(
                AuthOptions.ISSUER,
                AuthOptions.AUDIENCE,
                claims,
                expires: expires,
                signingCredentials: creds
            );

            return Json(new
            {
                login = user.UserName,
                name = $"{user.FirstName} {user.LastName}",
                token = new JwtSecurityTokenHandler().WriteToken(token)
            });
        }

        [HttpGet("check")]
        public async Task<IActionResult> Check()
        {
            return Json(User.Claims.Select(c => new { Type = c.Type, Value = c.Value }).ToArray());
        }

        public class AuthModel
        {
            public string Login { get; set; }
            public string Password { get; set; }
        }
    }
}
