﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tonakai.Contexts;
using Tonakai.Models.API.File;
using Tonakai.Services;

namespace Tonakai.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FileController : Controller
    {
        private readonly FileDataService _files;

        public FileController(FileDataService files)
        {
            _files = files;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> Get(string id, bool dl = false)
        {
            try
            {
                var (data, info) = await _files.GetFile(id);

                if (!dl && (info.MimeType.StartsWith("image") || info.MimeType.StartsWith("video") || info.MimeType.StartsWith("audio")))
                    return File(data, info.MimeType);
                return File(data, info.MimeType, info.Name);
            }
            catch (DataServiceException e)
            {
                return NotFound();
            }
        }

        [HttpGet("{id}/Info")]
        public async Task<ActionResult<FileInfo>> GetInfo(string id)
        {
            try
            {
                return Ok(await _files.GetFileInfo(id));
            }
            catch (DataServiceException e)
            {
                return BadRequest(e.ApiError);
            }
        }

        [HttpPost]
        [RequestSizeLimit(25_000_000)]
        public async Task<ActionResult<FileInfo>> Post(FileCreate file)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            try
            {
                return Ok(await _files.CreateFileAsync(file));
            }
            catch (DataServiceException e)
            {
                return BadRequest(e.ApiError);
            }
        }
    }
}
