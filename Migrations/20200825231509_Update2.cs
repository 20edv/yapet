﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Tonakai.Migrations
{
    public partial class Update2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CheckNoteSection_CheckNotes_CheckNoteId",
                table: "CheckNoteSection");

            migrationBuilder.DropForeignKey(
                name: "FK_CheckNoteSection_Sections_SectionId",
                table: "CheckNoteSection");

            migrationBuilder.DropForeignKey(
                name: "FK_Files_CheckNotes_CheckNoteId",
                table: "Files");

            migrationBuilder.DropIndex(
                name: "IX_Files_CheckNoteId",
                table: "Files");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CheckNoteSection",
                table: "CheckNoteSection");

            migrationBuilder.DropColumn(
                name: "CheckNoteId",
                table: "Files");

            migrationBuilder.RenameTable(
                name: "CheckNoteSection",
                newName: "CheckNoteSections");

            migrationBuilder.RenameIndex(
                name: "IX_CheckNoteSection_SectionId",
                table: "CheckNoteSections",
                newName: "IX_CheckNoteSections_SectionId");

            migrationBuilder.RenameIndex(
                name: "IX_CheckNoteSection_CheckNoteId",
                table: "CheckNoteSections",
                newName: "IX_CheckNoteSections_CheckNoteId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CheckNoteSections",
                table: "CheckNoteSections",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CheckNoteSections_CheckNotes_CheckNoteId",
                table: "CheckNoteSections",
                column: "CheckNoteId",
                principalTable: "CheckNotes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CheckNoteSections_Sections_SectionId",
                table: "CheckNoteSections",
                column: "SectionId",
                principalTable: "Sections",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CheckNoteSections_CheckNotes_CheckNoteId",
                table: "CheckNoteSections");

            migrationBuilder.DropForeignKey(
                name: "FK_CheckNoteSections_Sections_SectionId",
                table: "CheckNoteSections");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CheckNoteSections",
                table: "CheckNoteSections");

            migrationBuilder.RenameTable(
                name: "CheckNoteSections",
                newName: "CheckNoteSection");

            migrationBuilder.RenameIndex(
                name: "IX_CheckNoteSections_SectionId",
                table: "CheckNoteSection",
                newName: "IX_CheckNoteSection_SectionId");

            migrationBuilder.RenameIndex(
                name: "IX_CheckNoteSections_CheckNoteId",
                table: "CheckNoteSection",
                newName: "IX_CheckNoteSection_CheckNoteId");

            migrationBuilder.AddColumn<int>(
                name: "CheckNoteId",
                table: "Files",
                type: "integer",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_CheckNoteSection",
                table: "CheckNoteSection",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_Files_CheckNoteId",
                table: "Files",
                column: "CheckNoteId");

            migrationBuilder.AddForeignKey(
                name: "FK_CheckNoteSection_CheckNotes_CheckNoteId",
                table: "CheckNoteSection",
                column: "CheckNoteId",
                principalTable: "CheckNotes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CheckNoteSection_Sections_SectionId",
                table: "CheckNoteSection",
                column: "SectionId",
                principalTable: "Sections",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Files_CheckNotes_CheckNoteId",
                table: "Files",
                column: "CheckNoteId",
                principalTable: "CheckNotes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
