﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Tonakai.Migrations
{
    public partial class Update4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "Iteration",
                table: "CheckNotes",
                nullable: false,
                defaultValue: 1L);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Iteration",
                table: "CheckNotes");
        }
    }
}
