﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Tonakai.Migrations
{
    public partial class Update1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CheckNoteCheckListItems",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CheckNoteId = table.Column<int>(nullable: false),
                    CheckListItemId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CheckNoteCheckListItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CheckNoteCheckListItems_CheckListItems_CheckListItemId",
                        column: x => x.CheckListItemId,
                        principalTable: "CheckListItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CheckNoteCheckListItems_CheckNotes_CheckNoteId",
                        column: x => x.CheckNoteId,
                        principalTable: "CheckNotes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CheckNoteCheckListItems_CheckListItemId",
                table: "CheckNoteCheckListItems",
                column: "CheckListItemId");

            migrationBuilder.CreateIndex(
                name: "IX_CheckNoteCheckListItems_CheckNoteId",
                table: "CheckNoteCheckListItems",
                column: "CheckNoteId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CheckNoteCheckListItems");
        }
    }
}
