﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tonakai.Models.DB;

namespace Tonakai.Models.Report
{
    public class CheckNoteInfo
    {
        //public CheckNote Note { get; set; }

        public API.Notes.NoteInfo Note;

        public int Id { get => Note.Id; }

        public string Status
        {
            get
            {
                switch (Note.Status)
                {
                    case CheckNote.StatusEnum.Actual: return "Актуально";
                    case CheckNote.StatusEnum.Closed: return "Принято";
                    case CheckNote.StatusEnum.Deleted: return "Удалено";
                    case CheckNote.StatusEnum.ReqCheck: return "Исправлено";
                    default: return "Неизв.";
                }
            }
        }

        public string Title
        {
            get => Note.Title.Name;
        }

        public string Sections
        {
            get => string.Join(", ", Note.Sections.Select(s => s.Name).ToArray());
        }

        public string Equipment
        {
            get => string.Join(", ", Note.Equipment.Select(s => s.Name).ToArray());
        }

        public string ShortText
        {
            get => Note.ShortText;
        }

        public string CreatedBy
        {
            get => $"{Note.CreatedBy.LastName} {Note.CreatedBy.FirstName}";
        }

        public string AnsweredBy
        {
            get => Note.AnsweredBy != null ? $"{Note.AnsweredBy.LastName} {Note.AnsweredBy.FirstName}" : "-";
        }

        public DateTime CreateTime
        {
            get => Note.CreateDate;
        }

        public DateTime UpdateTime
        {
            get => Note.UpdateDate;
        }
    }
}
