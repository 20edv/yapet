﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Tonakai.Models.API.File
{
    public class FileInfo
    {
        public FileInfo() { }
        public FileInfo(DB.File file)
        {
            Id = file.Id;
            Name = file.Name;
            MimeType = file.MimeType;
            Size = file.Size;
        }

        [Required]
        public string Id { get; set; }

        public string Name { get; set; }

        public string MimeType { get; set; }

        public int Size { get; set; }
    }
}
