﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Tonakai.Models.API.File
{
    public class FileCreate
    {
        [Required]

        public string Name { get; set; }

        [Required]
        public string Data { get; set; }
    }
}
