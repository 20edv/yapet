﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Tonakai.Models.API.Equipment;

namespace Tonakai.Models.API.Section
{
    public class SectionCreate
    {
        [Required]
        public int Project { get; set; }

        [Required]
        public string Name { get; set; }

        public EquipmentCreate[] Equipment { get; set; }
    }
}
