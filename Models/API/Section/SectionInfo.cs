﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Tonakai.Models.API.Equipment;

namespace Tonakai.Models.API.Section
{
    public class SectionInfo
    {
        public SectionInfo() { }
        public SectionInfo(DB.Section section, bool useEquip = false)
        {
            Id = section.Id;
            Name = section.Name;
            if (useEquip)
                Equipment = section.Equipment.Select(e => new EquipmentInfo(e)).ToArray();
        }

        public SectionInfo(DB.Section section, EquipmentInfo[] equipment)
        {
            Id = section.Id;
            Name = section.Name;
            Equipment = equipment;
        }

        [Required]
        public int Id { get; set; }

        public string Name { get; set; }

        public EquipmentInfo[] Equipment { get; set; }
    }
}
