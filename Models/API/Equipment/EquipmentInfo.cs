﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace Tonakai.Models.API.Equipment
{
    public class EquipmentInfo
    {
        public EquipmentInfo() { }
        public EquipmentInfo(DB.Equipment equip)
        {
            Id = equip.Id;
            Name = equip.Name;
            Data = equip.Data.RootElement;
        }

        [Required]
        public int Id { get; set; }

        public string Name { get; set; }

        public JsonElement? Data { get; set; }
    }
}
