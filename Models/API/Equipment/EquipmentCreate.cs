﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace Tonakai.Models.API.Equipment
{
    public class EquipmentCreate
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public JsonElement Data { get; set; }

        public int? Section { get; set; }
    }
}
