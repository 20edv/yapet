﻿using System.ComponentModel.DataAnnotations;

namespace Tonakai.Models.API.User
{
    public class UserInfo
    {
        [Required]
        public string Id { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MidName { get; set; }

        public string Department { get; set; }

        public string Login { get; set; }
        public string Email { get; set; }

        public UserInfo() { }

        public UserInfo(DB.User user)
        {
            Id = user.Id;
            FirstName = user.FirstName;
            LastName = user.LastName;
            MidName = user.MidName;
            Department = user.Department;
            Login = user.UserName;
            Email = user.Email;
        }
    }
}
