﻿using System.ComponentModel.DataAnnotations;

namespace Tonakai.Models.API.User
{
    public class UserCreate
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        public string MidName { get; set; }

        public string Department { get; set; }

        [Required]
        public string Login { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
