﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using static Tonakai.Models.DB.CheckNote;

namespace Tonakai.Models.API.Notes
{
    public class NoteCreate
    {
        [Required]
        public int Project { get; set; }

        [Required]
        public string Text { get; set; }
        
        public string ShortText { get; set; }

        [Required]
        public byte Priority { get; set; }

        [Required]
        public int Title { get; set; }

        [Required]
        public int[] Sections { get; set; }

        [Required]
        public int[] Equipment { get; set; }

        [Required]
        public int[] CheckListItems { get; set; }

        [Required]
        public string[] Files { get; set; }
    }
}
