using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.Json;
using Tonakai.Models.API.CheckList;
using Tonakai.Models.API.Equipment;
using Tonakai.Models.API.File;
using Tonakai.Models.API.Section;
using Tonakai.Models.API.Titles;
using Tonakai.Models.API.User;
using static Tonakai.Models.DB.CheckNote;

namespace Tonakai.Models.API.Notes
{
    public class NoteInfo
    {
        public NoteInfo() { }

        public NoteInfo(DB.CheckNote note, bool outData = false, bool outEvents = false)
        {
            Id = note.Id;
            Text = note.Text;
            ShortText = note.ShortText;
            Priority = note.Priority;
            Status = note.Status;
            CreateDate = note.CreateDate;
            UpdateDate = note.UpdateDate;
            CreatedBy = new UserInfo(note.CreatedBy);
            AnsweredBy = note.AnsweredBy == null ? null : new UserInfo(note.AnsweredBy);
            Iteration = note.Iteration;
            if (outData)
            {
                Title = new TitleInfo(note.Title);
                Sections = note.Sections.Select(s => new SectionInfo(s.Section)).ToArray();
                Equipment = note.Equipment.Select(e => new EquipmentInfo(e.Equipment)).ToArray();
                CheckListItems = note.CheckListItems.Select(i => new CheckListItemInfo(i.Item)).ToArray();
                Files = note.Files.Select(f => new FileInfo(f.File)).ToArray();
            }
            if (outEvents)
            {
                Events = note.Events.Select(e => new NoteEventInfo(e)).ToArray();
            }
        }

        [Required]
        public int Id { get; set; }

        public string Text { get; set; }

        public string ShortText { get; set; }

        public byte Priority { get; set; }

        public uint Iteration { get; set; } = 1;

        public TitleInfo Title { get; set; }

        public SectionInfo[] Sections { get; set; }

        public EquipmentInfo[] Equipment { get; set; }

        public CheckListItemInfo[] CheckListItems { get; set; }

        public StatusEnum Status { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime UpdateDate { get; set; }

        public UserInfo CreatedBy { get; set; }

        public UserInfo AnsweredBy { get; set; }

        public FileInfo[] Files { get; set; }

        public NoteEventInfo[] Events { get; set; }
    }

    public class NoteEventInfo
    {
        public NoteEventInfo() { }

        public NoteEventInfo(DB.CheckNoteEvent @event)
        {
            Id = @event.Id;
            User = new UserInfo(@event.User);
            Time = @event.Time;
            Type = @event.Type;
            Data = @event.Data.RootElement;
            Files = @event.Files.Select(f => new FileInfo(f.File)).ToArray();
        }

        public int Id { get; set; }

        public UserInfo User { get; set; }

        public DateTime Time { get; set; }

        public DB.CheckNoteEvent.EventType Type { get; set; }

        public JsonElement Data { get; set; }

        public FileInfo[] Files { get; set; }
    }
}
