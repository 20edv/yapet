﻿using System;
using System.Collections.Generic;
using System.Text;
using Tonakai.Models.API.File;

namespace Tonakai.Models.API.Notes
{
    public class NoteCommentCreate
    {
        public int NoteId { get; set; }
        public string Text { get; set; }
        public string[] Files { get; set; }
    }
}
