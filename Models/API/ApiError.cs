﻿using System.Collections.Generic;
using System.Linq;

namespace Tonakai.Models.API
{
    public class ApiError
    {
        public static class Common
        {
            public const string NotFound = "NOT_FOUND";
        }

        public ApiError(params string[] error)
        {
            Errors = error;
        }

        public ApiError(IEnumerable<Microsoft.AspNetCore.Identity.IdentityError> errors)
        {
            Errors = errors.Select(e => e.Code).ToArray();
        }

        public string[] Errors { get; set; }

        public static ApiError NotFound() => new ApiError(Common.NotFound);
    }
}
