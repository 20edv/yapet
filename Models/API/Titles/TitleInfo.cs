﻿using System.ComponentModel.DataAnnotations;

namespace Tonakai.Models.API.Titles
{
    public class TitleInfo
    {
        public TitleInfo() { }
        public TitleInfo(DB.TitleItem title)
        {
            Id = title.Id;
            Name = title.Name;
            Desc = title.Description;
        }

        [Required]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Desc { get; set; }
    }
}
