﻿using System.ComponentModel.DataAnnotations;

namespace Tonakai.Models.API.Titles
{
    public class TitleCreate
    {
        [Required]
        public int Project { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Desc { get; set; }
    }
}
