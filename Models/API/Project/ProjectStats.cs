﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tonakai.Models.API.Project
{
    public class ProjectStats
    {
        public string Name { get; set; }

        public NotesStats Notes { get; set; }

        public UserStats[] Users { get; set; }

        public class NotesStats
        {
            public int Actual { get; set; }
            public int ReqCheck { get; set; }
            public int Closed { get; set; }
        }

        public class UserStats
        {
            public User.UserInfo User { get; set; }
            public int Created { get; set; }
            public int Answered { get; set; }
        }
    }
}
