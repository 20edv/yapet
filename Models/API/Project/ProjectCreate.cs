﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Tonakai.Models.API.Project
{
    public class ProjectCreate
    {
        [Required]
        public string Name { get; set; }
    }
}
