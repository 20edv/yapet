﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Tonakai.Models.API.Project
{
    public class ProjectInfo
    {
        public ProjectInfo() { }
        public ProjectInfo(DB.Project project)
        {
            Id = project.Id;
            Name = project.Name;
        }

        [Required]
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
