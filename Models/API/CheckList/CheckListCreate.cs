﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Tonakai.Models.API.CheckList
{
    public class CheckListCreate
    {
        [Required]
        public int Project { get; set; }

        [Required]
        public string Name { get; set; }

        public CheckListItemCreate[] Items { get; set; }
    }

    public class CheckListItemCreate
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public byte Importance { get; set; }

        public int? CheckList { get; set; }
    }
}
