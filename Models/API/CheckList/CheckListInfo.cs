﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Tonakai.Models.API.CheckList
{
    public class CheckListInfo
    {
        public CheckListInfo() { }
        public CheckListInfo(DB.CheckList checklist, bool useItems = false)
        {
            Id = checklist.Id;
            Name = checklist.Name;
            if (useItems)
                Items = checklist.Items.Select(i => new CheckListItemInfo(i)).ToArray();
        }

        public CheckListInfo(DB.CheckList checklist, CheckListItemInfo[] items)
        {
            Id = checklist.Id;
            Name = checklist.Name;
            Items = items;
        }

        [Required]
        public int Id { get; set; }

        public string Name { get; set; }

        public CheckListItemInfo[] Items { get; set; }
    }

    public class CheckListItemInfo
    {
        public CheckListItemInfo() { }
        public CheckListItemInfo(DB.CheckListItem item)
        {
            Id = item.Id;
            Name = item.Name;
            Importance = item.Importance;
        }

        [Required]
        public int Id { get; set; }
        public string Name { get; set; }
        public byte? Importance { get; set; }
    }
}
