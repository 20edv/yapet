﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Tonakai.Models.DB
{
    public class CheckNoteEquipment
    {
        [Key]
        public int Id { get; set; }

        public int CheckNoteId { get; set; }

        public int EquipmentId { get; set; }

        [ForeignKey(nameof(CheckNoteId))]
        public virtual CheckNote CheckNote { get; set; }

        [ForeignKey(nameof(EquipmentId))]
        public virtual Equipment Equipment { get; set; }
    }
}
