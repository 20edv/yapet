﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tonakai.Models.DB
{
    /// <summary>
    /// Файл
    /// </summary>
    public class File
    {
        /// <summary>
        /// ID изображения
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }

        /// <summary>
        /// Название файла
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Данные
        /// </summary>
        public byte[] Data { get; set; }

        /// <summary>
        /// MIME-тип
        /// </summary>
        public string MimeType { get; set; }

        /// <summary>
        /// Размер файла
        /// </summary>
        public int Size { get; set; }

        /// <summary>
        /// Время загрузки
        /// </summary>
        public DateTime UploadDate { get; set; }

        /// <summary>
        /// Хеш файла
        /// </summary>
        public byte[] Hash { get; set; }
    }
}
