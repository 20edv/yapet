﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Tonakai.Models.DB
{
    public class CheckNoteCheckListItem
    {
        [Key]
        public int Id { get; set; }

        public int CheckNoteId { get; set; }

        public int CheckListItemId { get; set; }

        [ForeignKey(nameof(CheckNoteId))]
        public virtual CheckNote CheckNote { get; set; }

        [ForeignKey(nameof(CheckListItemId))]
        public virtual CheckListItem Item { get; set; }
    }
}
