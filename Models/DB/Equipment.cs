﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Text.Json;

namespace Tonakai.Models.DB
{
    /// <summary>
    /// Оборудование (позиция)
    /// </summary>
    public class Equipment
    {
        /// <summary>
        /// ID
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// ID раздела
        /// </summary>
        public int SectionId { get; set; }

        /// <summary>
        /// Раздел
        /// </summary>
        [ForeignKey(nameof(SectionId))]
        public Section Section { get; set; }

        /// <summary>
        /// Доп. данные
        /// </summary>
        [Column(TypeName = "jsonb")]
        public JsonDocument Data { get; set; }
    }
}
