﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tonakai.Models.DB
{
    /// <summary>
    /// Пункт чек-листа
    /// </summary>
    public class CheckListItem
    {
        /// <summary>
        /// ID пункта
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// ID чек-листа
        /// </summary>
        public int CheckListId { get; set; }

        /// <summary>
        /// Название пункта
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Важность пункта
        /// </summary>
        public byte Importance { get; set; }

        /// <summary>
        /// Чек-лист
        /// </summary>
        [ForeignKey(nameof(CheckListId))]
        public virtual CheckList CheckList { get; set; }
    }
}
