﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Tonakai.Models.DB
{
    public class CheckNoteFile
    {
        [Key]
        public int Id { get; set; }

        public int CheckNoteId { get; set; }

        public string FileId { get; set; }

        [ForeignKey(nameof(CheckNoteId))]
        public virtual CheckNote CheckNote { get; set; }

        [ForeignKey(nameof(FileId))]
        public virtual File File { get; set; }
    }
}
