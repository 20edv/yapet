﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Tonakai.Models.DB
{
    public class ReportTemplate
    {
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// ID проекта
        /// </summary>
        public int ProjectId { get; set; }

        /// <summary>
        /// Проект
        /// </summary>
        [ForeignKey(nameof(ProjectId))]
        public Project Project { get; set; }
        
        /// <summary>
        /// Запрос для отчета
        /// </summary>
        public string Query { get; set; }

        /// <summary>
        /// ID файла
        /// </summary>
        public string FileId { get; set; }

        /// <summary>
        /// Файл
        /// </summary>
        [ForeignKey(nameof(FileId))]
        public File File { get; set; }
    }
}
