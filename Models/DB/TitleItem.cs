﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tonakai.Models.DB
{
    /// <summary>
    /// Титул
    /// </summary>
    public class TitleItem
    {
        /// <summary>
        /// ID титула
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Название титула
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание титула
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// ID проекта
        /// </summary>
        public int ProjectId { get; set; }

        /// <summary>
        /// Проект
        /// </summary>
        [ForeignKey(nameof(ProjectId))]
        public Project Project { get; set; }
    }
}
