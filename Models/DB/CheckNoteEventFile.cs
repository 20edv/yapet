﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Tonakai.Models.DB
{
    public class CheckNoteEventFile
    {
        [Key]
        public int Id { get; set; }

        public int CheckNoteEventId { get; set; }

        public string FileId { get; set; }

        [ForeignKey(nameof(CheckNoteEventId))]
        public virtual CheckNoteEvent CheckNote { get; set; }

        [ForeignKey(nameof(FileId))]
        public virtual File File { get; set; }
    }
}
