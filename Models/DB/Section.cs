﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Tonakai.Models.DB
{
    /// <summary>
    /// Раздел
    /// </summary>
    public class Section
    {
        /// <summary>
        /// ID
        /// </summary>
        [Key]
        public int Id { get; set; }
        
        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// ID проекта
        /// </summary>
        public int ProjectId { get; set; }

        /// <summary>
        /// Проект
        /// </summary>
        [ForeignKey(nameof(ProjectId))]
        public Project Project { get; set; }

        /// <summary>
        /// Оборудование/позиция
        /// </summary>
        public virtual IEnumerable<Equipment> Equipment { get; set; }
    }
}
