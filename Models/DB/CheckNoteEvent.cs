﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Text.Json;

namespace Tonakai.Models.DB
{
    /// <summary>
    /// Событие
    /// </summary>
    public class CheckNoteEvent
    {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Id замечания
        /// </summary>
        public int NoteId { get; set; }

        /// <summary>
        /// Тип события
        /// </summary>
        public EventType Type { get; set; }

        /// <summary>
        /// Id пользователя
        /// </summary>
        public string? UserId { get; set; }

        /// <summary>
        /// Время
        /// </summary>
        public DateTime Time { get; set; }

        /// <summary>
        /// Данные
        /// </summary>
        [Column(TypeName = "jsonb")]
        public JsonDocument Data { get; set; }

        /// <summary>
        /// Файлы
        /// </summary>
        public virtual IEnumerable<CheckNoteEventFile> Files { get; set; }

        [ForeignKey(nameof(NoteId))]
        public virtual CheckNote Note { get; set; }

        [ForeignKey(nameof(UserId))]
        public virtual User User { get; set; }

        /// <summary>
        /// Типы событий
        /// </summary>
        public enum EventType : byte
        {
            /// <summary>
            /// Пустое событие
            /// </summary>
            Empty = 0,
            /// <summary>
            /// Добавление замечания
            /// </summary>
            Add = 1,
            /// <summary>
            /// Изменение замечания
            /// </summary>
            Edit = 2,
            /// <summary>
            /// Изменение статуса
            /// </summary>
            StatusChange = 3,
            /// <summary>
            /// Ответ
            /// </summary>
            Answer = 4,
            /// <summary>
            /// Комментарий
            /// </summary>
            Comment = 5,
            /// <summary>
            /// Удаление
            /// </summary>
            Delete = 6,
            /// <summary>
            /// Смена итерации
            /// </summary>
            IterationChange = 7
        }

        public class DataTypes
        {
            public class StatusChange
            {
                public CheckNote.StatusEnum From { get; set; }
                public CheckNote.StatusEnum To { get; set; }
            }

            public class Answer
            {
                public string Text { get; set; }
                public DateTime Time { get; set; }
                public string AnsweredBy { get; set; }
            }

            public class Comment
            {
                public string Text { get; set; }
            }

            public class IterationChange
            {
                public uint From { get; set; }
                public uint To { get; set; }
            }
        }
    }
}
