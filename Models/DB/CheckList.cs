﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tonakai.Models.DB
{
    /// <summary>
    /// Чек-лист
    /// </summary>
    public class CheckList
    {
        /// <summary>
        /// ID чек-листа
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Название чек-листа
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Пункты чек-листа
        /// </summary>
        public virtual IEnumerable<CheckListItem> Items { get; set; }

        /// <summary>
        /// ID проекта
        /// </summary>
        public int ProjectId { get; set; }

        /// <summary>
        /// Проект
        /// </summary>
        [ForeignKey(nameof(ProjectId))]
        public Project Project { get; set; }
    }
}
