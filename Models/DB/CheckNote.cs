﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tonakai.Models.DB
{
    /// <summary>
    /// Замечание
    /// </summary>
    public class CheckNote
    {
        /// <summary>
        /// ID замечания
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Текст замечания
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Краткое описание
        /// </summary>
        public string ShortText { get; set; }

        /// <summary>
        /// Титул
        /// </summary>
        [ForeignKey(nameof(TitleId))]
        public virtual TitleItem Title { get; set; }

        /// <summary>
        /// ID титула
        /// </summary>
        public int TitleId { get; set; }

        /// <summary>
        /// Разделы
        /// </summary>
        public virtual IEnumerable<CheckNoteSection> Sections { get; set; }

        /// <summary>
        /// Позиции
        /// </summary>
        public virtual IEnumerable<CheckNoteEquipment> Equipment { get; set; }

        /// <summary>
        /// Пункты чек-листов
        /// </summary>
        public virtual IEnumerable<CheckNoteCheckListItem> CheckListItems { get; set; }

        /// <summary>
        /// Приоритет
        /// </summary>
        public byte Priority { get; set; }

        /// <summary>
        /// Итерация
        /// </summary>
        public uint Iteration { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        public StatusEnum Status { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreateDate { get; set; }
        
        /// <summary>
        /// Дата последнего обновления
        /// </summary>
        public DateTime UpdateDate { get; set; }

        /// <summary>
        /// Пользователь создавший замечание
        /// </summary>
        [ForeignKey(nameof(CreatedById))]
        public virtual User CreatedBy { get; set; }

        /// <summary>
        /// ID пользователя создавшего замечание
        /// </summary>
        public string CreatedById { get; set; }

        /// <summary>
        /// Ответственный пользователь
        /// </summary>
        [ForeignKey(nameof(AnsweredById))]
        public virtual User AnsweredBy { get; set; }

        /// <summary>
        /// ID ответственного пользователя
        /// </summary>
        public string AnsweredById { get; set; }

        /// <summary>
        /// Файлы
        /// </summary>
        public virtual IEnumerable<CheckNoteFile> Files { get; set; }

        /// <summary>
        /// События
        /// </summary>
        public virtual IEnumerable<CheckNoteEvent> Events { get; set; }

        /// <summary>
        /// ID проекта
        /// </summary>
        public int ProjectId { get; set; }

        /// <summary>
        /// Проект
        /// </summary>
        [ForeignKey(nameof(ProjectId))]
        public Project Project { get; set; }

        /// <summary>
        /// Статусы
        /// </summary>
        public enum StatusEnum : byte
        {
            /// <summary>
            /// Актуальный
            /// </summary>
            Actual = 0,
            /// <summary>
            /// Ожидает проверки
            /// </summary>
            ReqCheck = 1,
            /// <summary>
            /// Закрыт
            /// </summary>
            Closed = 2,
            /// <summary>
            /// Удален
            /// </summary>
            Deleted = 255
        }
    }
}
