﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Tonakai
{
    public class MainHub : Hub
    {
        [HubMethodName("checklist:sub")]
        public async Task ChecklistSubscribe(int checklistId)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, $"checklist:{checklistId}");
        }

        [HubMethodName("checklist:unsub")]
        public async Task ChecklistUnsubscribe(int checklistId)
        {
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, $"checklist:{checklistId}");
        }
    }
}
