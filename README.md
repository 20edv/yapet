﻿# Tonakai

---

## Prerequisites

* [.NET Core](https://www.microsoft.com/net/download/windows) >= 3.1
* [NodeJS](https://nodejs.org/) >= 8.9
* [Vue CLI](https://cli.vuejs.org/) >= 4.0
* Your favourite editor (I prefer [VS Code](https://code.visualstudio.com/)), or VS 2017/19

---

## Run the application

You have three choices when it comes to how you prefer to run the app. You can either use the command line or the build-in run command.

### 1. Using the command line

* Run the .NET application using `dotnet run`

### 2. Using the built-in run command

* Run the application in VSCode or Visual Studio 2017 by hitting `F5`

> It will take some time during the first run to download all client side dependencies.

Browse to [http://localhost:5000](http://localhost:5000) for ASP.&#8203;NET Core + Vue app or browse to [http://localhost:8080](http://localhost:8080) for Vue app only.

## Publish the application

### 1. Folder output

* Run the .NET publish command using Release configuration: `dotnet publish -c Release`

or

* Follow the Publish wizard in Visual Studio selecting Folder profile.

### 2. Docker output

* Run the following command in a cmd window to build the docker image:
`docker build -t <IMAGE_NAME> .`

> ATTENTION! Do not miss the final dot to build the current directory

* Run the application in a cmd window by this command:
`docker run -d -p 5000:80 <IMAGE_NAME>`

## View your application running


## Recommended plugin for debugging Vue

* Get Chrome DevTools for Vue.js [here](https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd)