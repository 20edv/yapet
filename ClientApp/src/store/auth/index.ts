import { Module } from 'vuex';
import { getters } from './getters';
import { actions } from './actions';
import { mutations } from './mutations';
import { AuthState, AuthStateNull } from './types';
import { RootState } from '../types';

import axios from 'axios';

var conf = JSON.parse(localStorage.getItem("jwt") ?? JSON.stringify(AuthStateNull));
export const state: AuthState = {
  login: conf.login,
  name: conf.name,
  token: conf.token
};

const namespaced: boolean = true;

export const auth: Module<AuthState, RootState> = {
  namespaced,
  state,
  getters,
  actions,
  mutations,
};
