import { GetterTree } from 'vuex';
import { AuthState } from './types';
import { RootState } from '../types';

function parseJwt(token: string) {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
}

export const getters: GetterTree<AuthState, RootState> = {
    isAuth(state): boolean {
        return state.token != null && parseJwt(state.token).exp > new Date().getTime() / 1000;
    },
    token(state): string | null {
        if (state.token != null)
            return parseJwt(state.token).exp > new Date().getTime() / 1000 ? state.token : null;
        return state.token;
    },
    login(state): string | null {
        if (state.token != null)
            return parseJwt(state.token).exp > new Date().getTime() / 1000 ? state.login : null;
        return state.login;
    },
    name(state): string | null {
        if (state.token != null)
            return parseJwt(state.token).exp > new Date().getTime() / 1000 ? state.name : null;
        return state.name;
    }
};
