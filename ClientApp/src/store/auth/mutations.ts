import { MutationTree } from 'vuex';
import { AuthState } from './types';
import { stat } from 'fs';

export const mutations: MutationTree<AuthState> = {
  setToken(state, payload) {
    state.token = payload;
  },
  setName(state, payload: any) {
    state.name = payload;
  },
  setLogin(state, payload: any) {
    state.login = payload;
  },
};
