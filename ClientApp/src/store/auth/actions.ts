import { ActionTree } from 'vuex';
import axios from 'axios';
import { AuthState, AuthStateNull } from './types';
import { RootState } from '../types';
import { auth } from '.';

export const actions: ActionTree<AuthState, RootState> = {
  login({ commit }, auth: AuthState) {
    localStorage.setItem("jwt", JSON.stringify(auth));
    commit('setToken', auth.token);
    commit('setName', auth.name);
    commit('setLogin', auth.login);
  },
  logout({ commit }): any {
    localStorage.setItem("jwt", JSON.stringify(AuthStateNull));
    commit('setToken', null);
    commit('setName', null);
    commit('setLogin', null);
  },
  updateState({ commit }): any {
    var conf = JSON.parse(localStorage.getItem("jwt") ?? JSON.stringify(AuthStateNull));
    commit('setToken', conf.token);
    commit('setName', conf.name);
    commit('setLogin', conf.login);
  }
};
