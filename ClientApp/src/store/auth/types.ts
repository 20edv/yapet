export interface AuthState {
  token: string | null;
  login: string | null;
  name: string | null;
}

export let AuthStateNull: AuthState = {
  login: null,
  name: null,
  token: null
}