export interface Importance {
  id: number;
  name: string;
  color?: string;
}

export let ImportanceList: Importance[] = [
  {
    id: 10,
    name: "Низкий",
    color: "green"
  },
  {
    id: 20,
    name: "Средний",
    color: "yellow"
  },
  {
    id: 30,
    name: "Высокий",
    color: "orange"
  },
  {
    id: 40,
    name: "Очень высокий",
    color: "red"
  },
];