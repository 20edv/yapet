export let StatusInfo = {
    0: {
        id: 0,
        name: "Актуально",
        color: "red"
    },
    1: {
        id: 1,
        name: "Исправлено",
        color: "orange"
    },
    2: {
        id: 2,
        name: "Принято",
        color: "green"
    },
    255: {
        id: 255,
        name: "Удалено",
        color: "gray"
    }
}