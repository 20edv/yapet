import { TitleInfo } from '../title';
import { SectionInfo } from '../section';
import { EquipmentInfo } from '../equipment';
import { CheckListItemInfo } from '../checklist';
import { UserInfo } from '../user';
import { FileInfo } from '../file';

export interface NoteInfo {
  id: number;
  text: string;
  shortText: string;
  priority: number;
  iteration: number;
  title: TitleInfo;
  sections: SectionInfo[];
  equipment: EquipmentInfo[];
  checkListItems: CheckListItemInfo[];
  status: NoteStatus;
  createDate: Date;
  updateDate: Date;
  createdBy: UserInfo;
  answeredBy: UserInfo;
  files: FileInfo[];
  events: NoteEventInfo[];
}

export interface NoteEventInfo {
  id: number;
  user: UserInfo;
  time: Date;
  type: EventType;
  data: any;
  files: FileInfo[];
}

export enum NoteStatus {
  Actual = 0,
  ReqCheck = 1,
  Closed = 2,
  Deleted = 255
}

export enum EventType {
  Empty = 0,
  Add = 1,
  Edit = 2,
  StatusChange = 3,
  Answer = 4,
  Comment = 5,
  Delete = 6,
  IterationChange = 7
}