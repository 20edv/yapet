export interface NoteCreate {
  project: number;
  text: string;
  shortText: string;
  priority: number;
  title: number;
  sections: number[];
  equipment: number[];
  checkListItems: number[];
  files: string[];
}