export interface CheckListCreate {
  project: number;
  name: string;
  items: CheckListItemCreate[];
}

export interface CheckListItemCreate {
  name: string;
  importance: number;
  checkList?: number;
}