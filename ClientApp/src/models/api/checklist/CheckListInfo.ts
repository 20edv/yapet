export interface CheckListInfo {
  id: number;
  name: string;
  items: CheckListItemInfo[];
}

export interface CheckListItemInfo {
  id: number;
  name: string;
  importance: number;
}