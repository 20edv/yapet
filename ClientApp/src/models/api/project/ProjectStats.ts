import { UserInfo } from '../user';

export interface ProjectStats {
    name: string;
    notes: {
        actual: number;
        reqCheck: number;
        closed: number;
    },
    users: {
        user: UserInfo,
        created: number;
        answered: number;
    }[];
}