export interface TitleInfo {
  id: number;
  name: string;
  desc: string;
}