export interface TitleCreate {
  project: number;
  name: string;
  desc: string;
}