export interface FileCreate {
    name: string;
    data: string;
}