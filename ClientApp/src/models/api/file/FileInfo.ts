export interface FileInfo {
    id: string;
    name: string;
    mimeType: string;
    size: number;
}