import { UserInfo } from './UserInfo';

export function UserColor(u: UserInfo): string {
    return UserColorText(u.login);
}

export function UserColorText(login: string): string {
    var s = 100, l = 30;
    var hash = 0;
    for (var i = 0; i < login.length; i++) {
        hash = login.charCodeAt(i) + ((hash << 5) - hash);
    }

    var h = hash % 360;
    //return 'hsl(' + h + ', ' + s + '%, ' + l + '%)';
    return HSLToHex(h, s, l);
}

function HSLToHex(h: number, s: number, l: number): string {
    s /= 100;
    l /= 100;

    let c = (1 - Math.abs(2 * l - 1)) * s,
        x = c * (1 - Math.abs((h / 60) % 2 - 1)),
        m = l - c / 2,
        r = 0,
        g = 0,
        b = 0;

    if (0 <= h && h < 60) {
        r = c; g = x; b = 0;
    } else if (60 <= h && h < 120) {
        r = x; g = c; b = 0;
    } else if (120 <= h && h < 180) {
        r = 0; g = c; b = x;
    } else if (180 <= h && h < 240) {
        r = 0; g = x; b = c;
    } else if (240 <= h && h < 300) {
        r = x; g = 0; b = c;
    } else if (300 <= h && h < 360) {
        r = c; g = 0; b = x;
    }
    // Having obtained RGB, convert channels to hex
    let rs = Math.round((r + m) * 255).toString(16);
    let gs = Math.round((g + m) * 255).toString(16);
    let bs = Math.round((b + m) * 255).toString(16);

    // Prepend 0s, if necessary
    if (rs.length == 1)
        rs = "0" + rs;
    if (gs.length == 1)
        gs = "0" + gs;
    if (bs.length == 1)
        bs = "0" + bs;

    return "#" + rs + gs + bs;
}