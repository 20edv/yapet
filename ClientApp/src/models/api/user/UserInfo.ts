export interface UserInfo {
  id: string;
  firstName: string;
  lastName: string;
  midName?: string;
  department?: string;
  login: string;
  email: string;
}