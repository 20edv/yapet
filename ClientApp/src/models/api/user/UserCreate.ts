export class UserCreate {
  public firstName: string = "";
  public lastName: string = "";
  public midName: string = "";
  public department: string = "";
  public login: string = "";
  public email: string = "";
  public password: string = "";
}