export interface EquipmentInfo {
    id: number;
    name: string;
    data: any;
}