export interface EquipmentCreate {
    name: string;
    data: any;
    section?: number;
}