import { EquipmentInfo } from '../equipment';

export interface SectionInfo {
    id: number;
    name: string;
    equipment: EquipmentInfo[];
}