import { EquipmentCreate } from '../equipment';

export interface SectionCreate {
    project: number;
    name: string;
    equipment?: EquipmentCreate[];
}