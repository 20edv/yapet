export default (name: string) => {
    return name
        .split(" ")
        .flatMap(val => (val.length != 0 ? val[0].toUpperCase() : ""))
        .join("");
};
