import { formatRelative, subDays } from 'date-fns'
import { ru } from 'date-fns/locale'

export default (date: Date) => {
    if (typeof(date) == "string")
        date = new Date(Date.parse(date));
    return formatRelative(date, new Date(), { locale: ru });
};
