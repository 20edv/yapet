export function markdownPrerender(text: string): string {
    let noteReg = /\[note=(\d+)\]/gim;
    let copyReg1 = /\[copy=(\d+)\]/gim;
    let copyReg2 = /\[copy\]/gim;
    return text
        .replace(noteReg, "[Замечание #$1](/notes/$1)")
        .replace(copyReg1, "<a href='/notes/$1'><span class='mx-1 white--text v-chip theme--light v-size--small orange'><span class='v-chip__content'>Дубликат замечания #$1</span></span></a>")
        .replace(copyReg2, "<span class='mx-1 white--text v-chip theme--light v-size--small orange'><span class='v-chip__content'>Дубликат</span></span>");
}