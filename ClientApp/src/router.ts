import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Root from './views/Root.vue';
import ProjectsRoot from './views/ProjectsRoot.vue';
import Auth from './views/Auth.vue';

import store from './store';

Vue.use(Router);

let router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      component: ProjectsRoot,
      meta: {
        requiresAuth: true
      },
      children: [
        // {
        //   path: '/',
        //   name: 'home',
        //   component: Home
        // },
        {
          path: '/',
          name: 'projects',
          component: () => import('./views/Projects/Projects.vue')
        },
        {
          path: 'settings',
          name: 'settings',
          component: () => import(/* webpackChunkName: "settings" */ './views/Settings.vue'),
          children: [
            { path: '', redirect: 'users' },
            { path: 'users', component: () => import(/* webpackChunkName: "settings" */ './views/Settings/Users.vue') }
          ]
        },
      ]
    },
    {
      path: '/project/:projectId',
      component: Root,
      meta: {
        requiresAuth: true
      },
      children: [
        {
          path: '',
          redirect: 'notes'
        },
        {
          path: 'dashboard',
          redirect: 'notes',
          name: 'dashboard',
          //   component: Home
        },
        {
          path: 'settings',
          name: 'projectSettings',
          component: () => import(/* webpackChunkName: "settings" */ './views/ProjectSettings.vue'),
          children: [
            { path: '', redirect: 'project' },
            { path: 'project', component: () => import(/* webpackChunkName: "settings" */ './views/Settings/Project.vue') },
            { path: 'titles', component: () => import(/* webpackChunkName: "settings" */ './views/Settings/Titles.vue') },
            { path: 'sections', component: () => import(/* webpackChunkName: "settings" */ './views/Settings/Sections.vue') },
            { path: 'checklists', component: () => import(/* webpackChunkName: "settings" */ './views/Settings/CheckLists.vue') }
          ]
        },
        {
          path: 'notes',
          name: 'notes',
          component: () => import(/* webpackChunkName: "notes" */ './views/Notes/Notes.vue')
        },
        {
          path: 'notes/:id',
          name: 'note',
          component: () => import(/* webpackChunkName: "notes" */ './views/Notes/Note.vue')
        },
        {
          path: 'stats',
          name: 'stats',
          component: () => import(/* webpackChunkName: "projectstats" */ './views/Projects/ProjectStats.vue')
        }
      ]
    },
    {
      path: '/auth',
      name: 'auth',
      component: Auth
    }
  ],
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    console.log("Is Auth: " + store.getters["auth/isAuth"]);
    if (store.getters["auth/isAuth"]) {
      next();
      return;
    }
    next('/auth?to=' + to.fullPath);
  }
  next();
});

export default router;