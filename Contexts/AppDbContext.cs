﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Tonakai.Models.DB;

namespace Tonakai.Contexts
{
    public class AppDbContext : IdentityDbContext<User>
    {
        /// <summary>
        /// Чек-листы
        /// </summary>
        public DbSet<CheckList> CheckLists { get; set; }

        /// <summary>
        /// Пункты чек-листов
        /// </summary>
        public DbSet<CheckListItem> CheckListItems { get; set; }

        /// <summary>
        /// Замечания проверок
        /// </summary>
        public DbSet<CheckNote> CheckNotes { get; set; }

        /// <summary>
        /// Замечание
        /// </summary>
        public DbSet<CheckNoteEvent> CheckNoteEvents { get; set; }

        /// <summary>
        /// Изображения замечаний
        /// </summary>
        public DbSet<File> Files { get; set; }

        /// <summary>
        /// Титулы
        /// </summary>
        public DbSet<TitleItem> TitleItems { get; set; }

        /// <summary>
        /// Оборудование (позиции)
        /// </summary>
        public DbSet<Equipment> Equipment { get; set; }

        /// <summary>
        /// Разделы
        /// </summary>
        public DbSet<Section> Sections { get; set; }

        public DbSet<CheckNoteSection> CheckNoteSections { get; set; }

        public DbSet<CheckNoteEquipment> CheckNoteEquipment { get; set; }

        public DbSet<CheckNoteFile> CheckNoteFiles { get; set; }

        public DbSet<CheckNoteEventFile> CheckNoteEventFiles { get; set; }

        public DbSet<ReportTemplate> ReportTemplates { get; set; }

        public DbSet<Project> Projects { get; set; }

        public DbSet<CheckNoteCheckListItem> CheckNoteCheckListItems { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
#if RELEASE
            Database.Migrate();
#endif
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<CheckNote>().Property(nameof(CheckNote.Iteration)).HasDefaultValue(1);
        }
    }
}
