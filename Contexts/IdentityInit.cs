﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tonakai.Models.DB;

namespace Tonakai.Contexts
{
    public static class IdentityInit
    {
        public static void SeedData(UserManager<User> userManager, RoleManager<IdentityRole> roleManager)
        {
            SeedRoles(roleManager);
            SeedUsers(userManager);
        }

        public static void SeedUsers(UserManager<User> userManager)
        {
            if (userManager.Users.Count() == 0)
            {
                var user = new User
                {
                    FirstName = "Администратор",
                    LastName = "",
                    UserName = "admin",
                    Email = "admin@tonakai"
                };
                var res = userManager.CreateAsync(user, "$up3rU$3r").Result;
                if (res.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "SuperAdmin").Wait();
                }
            }
        }

        public static void SeedRoles(RoleManager<IdentityRole> roleManager)
        {
            if (!roleManager.RoleExistsAsync("SuperAdmin").Result)
            {
                IdentityRole role = new IdentityRole("SuperAdmin");
                var res = roleManager.CreateAsync(role).Result;
            }
            if (!roleManager.RoleExistsAsync("Admin").Result)
            {
                IdentityRole role = new IdentityRole("Admin");
                var res = roleManager.CreateAsync(role).Result;
            }
        }
    }
}
