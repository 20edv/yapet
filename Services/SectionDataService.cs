﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tonakai.Contexts;
using Tonakai.Models.API;
using Tonakai.Models.API.Section;
using Tonakai.Models.DB;

namespace Tonakai.Services
{
    public class SectionDataService
    {
        private readonly AppDbContext _context;
        private readonly EquipmentDataService _equip;

        public SectionDataService(AppDbContext context, EquipmentDataService equip)
        {
            _context = context;
            _equip = equip;
        }

        public async Task<SectionInfo[]> GetSectionsAsync(int project, bool useEquip = true)
        {
            return useEquip
                ? await _context.Sections.Where(s => s.ProjectId == project).Include(s => s.Equipment).Select(s => new SectionInfo(s, useEquip)).ToArrayAsync()
                : await _context.Sections.Where(s => s.ProjectId == project).Select(s => new SectionInfo(s, useEquip)).ToArrayAsync();
        }

        public async Task<SectionInfo[]> GetSectionsAsync(int project, int count, int offset = 0, bool useEquip = true)
        {
            return useEquip
                ? await _context.Sections.Where(s => s.ProjectId == project).Include(s => s.Equipment).Skip(offset).Take(count).Select(s => new SectionInfo(s, useEquip)).ToArrayAsync()
                : await _context.Sections.Where(s => s.ProjectId == project).Skip(offset).Take(count).Select(s => new SectionInfo(s, useEquip)).ToArrayAsync();
        }

        public async Task<SectionInfo> GetSectionAsync(int id, bool useEquip = true)
        {
            var proj = await _context.Sections.Where(s => s.Id == id).Select(s => new SectionInfo(s, useEquip)).FirstOrDefaultAsync();

            if (proj == null)
                throw new DataServiceException(ApiError.Common.NotFound);

            return proj;
        }

        public async Task<SectionInfo> CreateSectionAsync(SectionCreate section)
        {
            if (!await _context.Projects.AnyAsync(p => p.Id == section.Project))
                throw new DataServiceException("PROJECT_NOT_FOUND");

            var sec = new Section
            {
                Name = section.Name,
                ProjectId = section.Project
            };

            await _context.Sections.AddAsync(sec);
            await _context.SaveChangesAsync();

            if (section.Equipment != null)
            {
                var equip = await _equip.CreateEquipmentRangeAsync(sec.Id, section.Equipment);
                return new SectionInfo(sec, equip);
            }
            else
                return new SectionInfo(sec);
        }

        public async Task<SectionInfo> ChangeSectionAsync(SectionInfo section)
        {
            var sec = await _context.Sections.FirstOrDefaultAsync(section => section.Id == section.Id);

            if (sec == null)
                throw new DataServiceException(ApiError.Common.NotFound);

            if (section.Name != null)
                sec.Name = section.Name;

            await _context.SaveChangesAsync();

            return new SectionInfo(sec);
        }

        public async Task DeleteSectionAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task RecoverSectionAsync(int id)
        {
            throw new NotImplementedException();
        }
    }
}
