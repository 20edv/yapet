﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Tonakai.Contexts;
using Tonakai.Models.API;
using Tonakai.Models.API.Notes;
using Tonakai.Models.DB;

namespace Tonakai.Services
{
    public class NoteDataService
    {
        private readonly AppDbContext _context;

        public NoteDataService(AppDbContext context)
        {
            _context = context;
        }

        public async Task<NoteInfo[]> GetNotesAsync(int projectId, bool data = false, bool events = false, bool deleted = false)
        {
            return await BuildRequest(_context.CheckNotes, data, events, deleted).Where(n => n.ProjectId == projectId).Select(n => new NoteInfo(n, data, events)).ToArrayAsync();
        }

        public async Task<NoteInfo[]> GetNotesAsync(int projectId, int count, int offset = 0, bool data = true, bool events = false, bool deleted = false)
        {
            return await BuildRequest(_context.CheckNotes, data, events, deleted).Where(n => n.ProjectId == projectId).Skip(offset).Take(count).Select(n => new NoteInfo(n, data, events)).ToArrayAsync();
        }

        public async Task<NoteInfo[]> GetNotesAsync(bool data = false, bool events = false, bool deleted = false)
        {
            return await BuildRequest(_context.CheckNotes, data, events, deleted).Select(n => new NoteInfo(n, data, events)).ToArrayAsync();
        }

        public async Task<NoteInfo[]> GetNotesAsync(int count, int offset = 0, bool data = true, bool events = false, bool deleted = false)
        {
            return await BuildRequest(_context.CheckNotes, data, events, deleted).Skip(offset).Take(count).Select(n => new NoteInfo(n, data, events)).ToArrayAsync();
        }

        public async Task<NoteInfo> GetNoteAsync(int id, bool data = true, bool events = true)
        {
            var note = await BuildRequest(_context.CheckNotes.Where(n => n.Id == id), data, events, true).Select(n => new NoteInfo(n, data, events)).FirstOrDefaultAsync();

            if (note == null)
                throw new DataServiceException(ApiError.Common.NotFound);

            return note;
        }

        public async Task<NoteInfo> CreateNoteAsync(string userId, NoteCreate note)
        {
            if (!await _context.TitleItems.AnyAsync(t => t.Id == note.Title))
                throw new DataServiceException("TITLE_NOT_FOUND");
            if (await _context.Sections.CountAsync(s => note.Sections.Contains(s.Id)) != note.Sections.Distinct().Count())
                throw new DataServiceException("SECTION_NOT_FOUND");
            if (await _context.Equipment.CountAsync(e => note.Equipment.Contains(e.Id)) != note.Equipment.Distinct().Count())
                throw new DataServiceException("EQUIPMENT_NOT_FOUND");
            if (await _context.CheckListItems.CountAsync(i => note.CheckListItems.Contains(i.Id)) != note.CheckListItems.Distinct().Count())
                throw new DataServiceException("CHECKITEM_NOT_FOUND");
            if (await _context.Files.CountAsync(i => note.Files.Contains(i.Id)) != note.Files.Distinct().Count())
                throw new DataServiceException("FILE_NOT_FOUND");

            var nt = new CheckNote
            {
                ProjectId = note.Project,
                CreatedById = userId,
                CreateDate = DateTime.Now,
                UpdateDate = DateTime.Now,
                Text = note.Text,
                ShortText = note.ShortText,
                Priority = note.Priority,
                TitleId = note.Title
            };

            await _context.CheckNotes.AddAsync(nt);
            await _context.SaveChangesAsync();

            await _context.CheckNoteSections.AddRangeAsync(note.Sections.Select(s => new CheckNoteSection { CheckNoteId = nt.Id, SectionId = s }).ToArray());
            await _context.CheckNoteEquipment.AddRangeAsync(note.Equipment.Select(e => new CheckNoteEquipment { CheckNoteId = nt.Id, EquipmentId = e }).ToArray());
            await _context.CheckNoteCheckListItems.AddRangeAsync(note.CheckListItems.Select(i => new CheckNoteCheckListItem { CheckNoteId = nt.Id, CheckListItemId = i }).ToArray());
            await _context.CheckNoteFiles.AddRangeAsync(note.Files.Select(f => new CheckNoteFile { CheckNoteId = nt.Id, FileId = f }).ToArray());

            await _context.CheckNoteEvents.AddAsync(new CheckNoteEvent
            {
                NoteId = nt.Id,
                Type = CheckNoteEvent.EventType.Add,
                UserId = userId,
                Time = nt.UpdateDate,
                Data = JsonDocument.Parse("{}")
            });

            await _context.SaveChangesAsync();

            return await GetNoteAsync(nt.Id, true, true);
        }

        public async Task<NoteInfo> UpdateNoteAsync(string userId, NoteInfo note)
        {
            var nt = await _context.CheckNotes.FirstOrDefaultAsync(t => t.Id == note.Id);
            if (nt == null)
                throw new DataServiceException("NOT_FOUND");
            if (note.Title != null && !await _context.TitleItems.AnyAsync(t => t.Id == note.Title.Id))
                throw new DataServiceException("TITLE_NOT_FOUND");
            if (note.Sections != null && await _context.Sections.CountAsync(s => note.Sections.Select(a => a.Id).Contains(s.Id)) != note.Sections.Distinct().Count())
                throw new DataServiceException("SECTION_NOT_FOUND");
            if (note.Equipment != null && await _context.Equipment.CountAsync(e => note.Equipment.Select(a => a.Id).Contains(e.Id)) != note.Equipment.Distinct().Count())
                throw new DataServiceException("EQUIPMENT_NOT_FOUND");
            if (note.CheckListItems != null && await _context.CheckListItems.CountAsync(i => note.CheckListItems.Select(a => a.Id).Contains(i.Id)) != note.CheckListItems.Distinct().Count())
                throw new DataServiceException("CHECKITEM_NOT_FOUND");
            if (note.Files != null && await _context.Files.CountAsync(i => note.Files.Select(a => a.Id).Contains(i.Id)) != note.Files.Distinct().Count())
                throw new DataServiceException("FILE_NOT_FOUND");

            if (note.Text != null)
                nt.Text = note.Text;
            if (note.ShortText != null)
                nt.ShortText = note.ShortText;
            if (note.Priority != null)
                nt.Priority = note.Priority;
            if (note.Title != null)
                nt.TitleId = note.Title.Id;
            nt.UpdateDate = DateTime.Now;

            if (note.Sections != null)
            {
                _context.CheckNoteSections.AddRange(note.Sections.Where(a => !_context.CheckNoteSections.Any(s => s.SectionId == a.Id && s.CheckNoteId == nt.Id)).Select(s => new CheckNoteSection { CheckNoteId = nt.Id, SectionId = s.Id }));
                _context.CheckNoteSections.RemoveRange(_context.CheckNoteSections.Where(s => !note.Sections.Select(a => a.Id).Contains(s.SectionId) && s.CheckNoteId == nt.Id));
            }
            if (note.Equipment != null)
            {
                _context.CheckNoteEquipment.AddRange(note.Equipment.Where(a => !_context.CheckNoteEquipment.Any(e => e.EquipmentId == a.Id && e.CheckNoteId == nt.Id)).Select(s => new CheckNoteEquipment { CheckNoteId = nt.Id, EquipmentId = s.Id }));
                _context.CheckNoteEquipment.RemoveRange(_context.CheckNoteEquipment.Where(e => !note.Equipment.Select(a => a.Id).Contains(e.EquipmentId) && e.CheckNoteId == nt.Id));
            }
            if (note.CheckListItems != null)
            {
                _context.CheckNoteCheckListItems.AddRange(note.CheckListItems.Where(a => !_context.CheckNoteCheckListItems.Any(i => i.CheckListItemId == a.Id && i.CheckNoteId == nt.Id)).Select(s => new CheckNoteCheckListItem { CheckNoteId = nt.Id, CheckListItemId = s.Id }));
                _context.CheckNoteCheckListItems.RemoveRange(_context.CheckNoteCheckListItems.Where(i => !note.CheckListItems.Select(a => a.Id).Contains(i.CheckListItemId) && i.CheckNoteId == nt.Id));
            }
            if (note.Files != null)
            {
                _context.CheckNoteFiles.AddRange(note.Files.Where(a => !_context.CheckNoteFiles.Any(f => f.FileId == a.Id && f.CheckNoteId == nt.Id)).Select(s => new CheckNoteFile { CheckNoteId = nt.Id, FileId = s.Id }));
                _context.CheckNoteFiles.RemoveRange(_context.CheckNoteFiles.Where(f => !note.Files.Select(a => a.Id).Contains(f.FileId) && f.CheckNoteId == nt.Id));
            }

            await _context.CheckNoteEvents.AddAsync(new CheckNoteEvent
            {
                NoteId = nt.Id,
                Type = CheckNoteEvent.EventType.Edit,
                UserId = userId,
                Time = nt.UpdateDate,
                Data = JsonDocument.Parse("{}")
            });

            await _context.SaveChangesAsync();

            return await GetNoteAsync(nt.Id, true, true);
        }

        public async Task<NoteEventInfo> ChangeNoteStatusAsync(string userId, int noteId, CheckNote.StatusEnum status)
        {
            var nt = await _context.CheckNotes.FirstOrDefaultAsync(t => t.Id == noteId);
            if (nt == null)
                throw new DataServiceException("NOT_FOUND");

            nt.UpdateDate = DateTime.Now;

            CheckNoteEvent @event;
            if (status != CheckNote.StatusEnum.Deleted)
                await _context.CheckNoteEvents.AddAsync(@event = new CheckNoteEvent
                {
                    NoteId = nt.Id,
                    Type = CheckNoteEvent.EventType.StatusChange,
                    UserId = userId,
                    Time = nt.UpdateDate,
                    Data = JsonDocument.Parse(JsonSerializer.Serialize(new CheckNoteEvent.DataTypes.StatusChange { From = nt.Status, To = status }))
                });
            else
                await _context.CheckNoteEvents.AddAsync(@event = new CheckNoteEvent
                {
                    NoteId = nt.Id,
                    Type = CheckNoteEvent.EventType.Delete,
                    UserId = userId,
                    Time = nt.UpdateDate,
                    Data = JsonDocument.Parse("{}")
                });

            nt.Status = status;

            await _context.SaveChangesAsync();

            //return new NoteEventInfo(@event);
            return _context.CheckNoteEvents.Where(e => e.Id == @event.Id).Include(e => e.User).Include(e => e.Files).Select(e => new NoteEventInfo(e)).FirstOrDefault();
        }

        public async Task<NoteEventInfo> AddCommentAsync(string userId, NoteCommentCreate comment, bool isAnswer = false)
        {
            var nt = await _context.CheckNotes.FirstOrDefaultAsync(t => t.Id == comment.NoteId);
            if (nt == null)
                throw new DataServiceException("NOT_FOUND");
            if (comment.Files != null && await _context.Files.CountAsync(i => comment.Files.Contains(i.Id)) != comment.Files.Distinct().Count())
                throw new DataServiceException("FILE_NOT_FOUND");

            nt.UpdateDate = DateTime.Now;
            var ev = new CheckNoteEvent
            {
                NoteId = nt.Id,
                Type = isAnswer ? CheckNoteEvent.EventType.Answer : CheckNoteEvent.EventType.Comment,
                UserId = userId,
                Time = nt.UpdateDate,
                Data = JsonDocument.Parse(JsonSerializer.Serialize(new CheckNoteEvent.DataTypes.Comment { Text = comment.Text }))
            };
            await _context.CheckNoteEvents.AddAsync(ev);
            if (isAnswer) { nt.AnsweredById = userId; nt.Status = CheckNote.StatusEnum.ReqCheck; }

            await _context.SaveChangesAsync();

            await _context.CheckNoteEventFiles.AddRangeAsync(comment.Files.Select(f => new CheckNoteEventFile { CheckNoteEventId = ev.Id, FileId = f }));

            await _context.SaveChangesAsync();

            return _context.CheckNoteEvents.Where(e => e.Id == ev.Id).Include(e => e.User).Include(e => e.Files).ThenInclude(f => f.File).Select(e => new NoteEventInfo(e)).FirstOrDefault();
        }

        public async Task<NoteEventInfo> ChangeIteration(string userId, int noteId, uint iteration)
        {
            var nt = await _context.CheckNotes.FirstOrDefaultAsync(t => t.Id == noteId);
            if (nt == null)
                throw new DataServiceException("NOT_FOUND");

            nt.UpdateDate = DateTime.Now;
            var ev = new CheckNoteEvent
            {
                NoteId = nt.Id,
                Type = CheckNoteEvent.EventType.IterationChange,
                UserId = userId,
                Time = nt.UpdateDate,
                Data = JsonDocument.Parse(JsonSerializer.Serialize(new CheckNoteEvent.DataTypes.IterationChange { From = nt.Iteration, To = iteration }))
            };
            nt.Iteration = iteration;

            await _context.CheckNoteEvents.AddAsync(ev);
            await _context.SaveChangesAsync();

            return _context.CheckNoteEvents.Where(e => e.Id == ev.Id).Include(e => e.User).Include(e => e.Files).ThenInclude(f => f.File).Select(e => new NoteEventInfo(e)).FirstOrDefault();
        }

        private IQueryable<CheckNote> BuildRequest(IQueryable<CheckNote> request, bool data, bool events, bool deleted = false)
        {
            IQueryable<CheckNote> req = request.Include(n => n.CreatedBy).Include(n => n.AnsweredBy);
            if (!deleted)
                req = req.Where(n => n.Status != CheckNote.StatusEnum.Deleted);
            if (data)
                req = req
                    .Include(n => n.Title)
                    .Include(n => n.Sections).ThenInclude(s => s.Section)
                    .Include(n => n.Equipment).ThenInclude(e => e.Equipment)
                    .Include(n => n.CheckListItems).ThenInclude(i => i.Item)
                    .Include(n => n.Files).ThenInclude(f => f.File);
            if (events)
                req = req.Include(n => n.Events).ThenInclude(e => e.Files).ThenInclude(f => f.File).Include(n => n.Events).ThenInclude(n => n.User);
            return req;
        }
    }
}
