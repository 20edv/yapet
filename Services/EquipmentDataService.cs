﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Tonakai.Contexts;
using Tonakai.Models.API;
using Tonakai.Models.API.Equipment;
using Tonakai.Models.DB;

namespace Tonakai.Services
{
    public class EquipmentDataService
    {
        private readonly AppDbContext _context;

        public EquipmentDataService(AppDbContext context)
        {
            _context = context;
        }

        public async Task<EquipmentInfo[]> GetProjectEquipmentAsync(int id)
        {
            var equip = await _context.Equipment.Where(e => e.Section.ProjectId == id).Select(e => new EquipmentInfo(e)).ToArrayAsync();

            return equip;
        }

        public async Task<EquipmentInfo[]> GetSectionEquipmentAsync(int id)
        {
            var equip = await _context.Equipment.Where(e => e.SectionId == id).Select(e => new EquipmentInfo(e)).ToArrayAsync();

            return equip;
        }

        public async Task<EquipmentInfo> GetEquipmentAsync(int id)
        {
            var equip = await _context.Equipment.Where(e => e.Id == id).Select(e => new EquipmentInfo(e)).FirstOrDefaultAsync();

            if (equip == null)
                throw new DataServiceException(ApiError.Common.NotFound);

            return equip;
        }

        public async Task<EquipmentInfo> CreateEquipmentAsync(EquipmentCreate equipment)
        {
            if (!equipment.Section.HasValue)
                throw new DataServiceException("SECTION_IS_REQUIRED");

            if (!await _context.Sections.AnyAsync(s => s.Id == equipment.Section.Value))
                throw new DataServiceException("SECTION_NOT_FOUND");

            return await CreateEquipmentAsync(equipment.Section.Value, equipment);
        }

        public async Task<EquipmentInfo> CreateEquipmentAsync(int sectionId, EquipmentCreate equipment)
        {
            var equip = new Equipment
            {
                Name = equipment.Name,
                Data = JsonDocument.Parse(equipment.Data.GetRawText()),
                SectionId = sectionId
            };

            await _context.Equipment.AddAsync(equip);
            await _context.SaveChangesAsync();

            return new EquipmentInfo(equip);
        }

        public async Task<EquipmentInfo[]> CreateEquipmentRangeAsync(int sectionId, params EquipmentCreate[] equipment)
        {
            var equip = new List<Equipment>();

            foreach (var e in equipment)
            {
                equip.Add(new Equipment
                {
                    Name = e.Name,
                    Data = JsonDocument.Parse(e.Data.GetRawText()),
                    SectionId = sectionId
                });
            }

            await _context.Equipment.AddRangeAsync(equip);
            await _context.SaveChangesAsync();

            return equip.Select(e => new EquipmentInfo(e)).ToArray();
        }

        public async Task<EquipmentInfo> ChangeEquipmentAsync(EquipmentInfo equipment)
        {
            var equip = await _context.Equipment.FirstOrDefaultAsync(p => p.Id == equipment.Id); 

            if (equip == null)
                throw new DataServiceException(ApiError.Common.NotFound);

            if (equipment.Name != null)
                equip.Name = equipment.Name;

            if (equipment.Data.HasValue)
                equip.Data = JsonDocument.Parse(equipment.Data.Value.GetRawText());

            await _context.SaveChangesAsync();

            return new EquipmentInfo(equip);
        }

        public async Task DeleteEquipmentAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task RecoverEquipmentAsync(int id)
        {
            throw new NotImplementedException();
        }
    }
}
