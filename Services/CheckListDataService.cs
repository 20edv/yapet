﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tonakai.Contexts;
using Tonakai.Models.API;
using Tonakai.Models.API.CheckList;
using Tonakai.Models.DB;

namespace Tonakai.Services
{
    public class CheckListDataService
    {
        private readonly AppDbContext _context;

        public CheckListDataService(AppDbContext context)
        {
            _context = context;
        }

        public async Task<CheckListInfo[]> GetCheckListsAsync(int project, bool full = true)
        {
            return full
                ? await _context.CheckLists.Where(s => s.ProjectId == project).Include(s => s.Items).Select(s => new CheckListInfo(s, full)).ToArrayAsync()
                : await _context.CheckLists.Where(s => s.ProjectId == project).Select(s => new CheckListInfo(s, full)).ToArrayAsync();
        }

        public async Task<CheckListInfo[]> GetCheckListsAsync(int project, int count, int offset = 0, bool full = true)
        {
            return full
                ? await _context.CheckLists.Where(s => s.ProjectId == project).Include(s => s.Items).Select(s => new CheckListInfo(s, full)).ToArrayAsync()
                : await _context.CheckLists.Where(s => s.ProjectId == project).Select(s => new CheckListInfo(s, full)).ToArrayAsync();
        }

        public async Task<CheckListInfo> GetCheckListAsync(int id)
        {
            var checklist = await _context.CheckLists.Where(s => s.Id == id).Include(s => s.Items).Select(s => new CheckListInfo(s, true)).FirstOrDefaultAsync();

            if (checklist == null)
                throw new DataServiceException(ApiError.Common.NotFound);

            return checklist;
        }

        public async Task<CheckListInfo> CreateCheckListAsync(CheckListCreate checklist)
        {
            if (!await _context.Projects.AnyAsync(s => s.Id == checklist.Project))
                throw new DataServiceException("PROJECT_NOT_FOUND");

            var clist = new CheckList
            {
                Name = checklist.Name,
                ProjectId = checklist.Project
            };

            await _context.CheckLists.AddAsync(clist);
            await _context.SaveChangesAsync();

            if (checklist.Items != null)
            {
                var items = await CreateCheckListItemRangeAsync(clist.Id, checklist.Items);
                return new CheckListInfo(clist, items);
            }
            else
                return new CheckListInfo(clist);
        }


        public async Task<CheckListInfo> ChangeCheckListAsync(CheckListInfo checklist)
        {
            var clist = await _context.CheckLists.Where(s => s.Id == checklist.Id).FirstOrDefaultAsync();

            if (clist == null)
                throw new DataServiceException(ApiError.Common.NotFound);

            if (checklist.Name != null)
                clist.Name = checklist.Name;

            await _context.CheckLists.AddAsync(clist);
            await _context.SaveChangesAsync();

            return new CheckListInfo(clist);
        }

        public async Task DeleteCheckListAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<CheckListItemInfo> CreateCheckListItemAsync(CheckListItemCreate checklistItem)
        {
            if (!checklistItem.CheckList.HasValue)
                throw new DataServiceException("CHECKLIST_IS_REQUIRED");

            if (!await _context.CheckLists.AnyAsync(s => s.Id == checklistItem.CheckList.Value))
                throw new DataServiceException("CHECKLIST_NOT_FOUND");

            return await CreateCheckListItemAsync(checklistItem.CheckList.Value, checklistItem);
        }

        public async Task<CheckListItemInfo> CreateCheckListItemAsync(int checklistId, CheckListItemCreate checklistItem)
        {
            var item = new CheckListItem
            {
                Name = checklistItem.Name,
                Importance = checklistItem.Importance,
                CheckListId = checklistId
            };

            await _context.CheckListItems.AddAsync(item);
            await _context.SaveChangesAsync();

            return new CheckListItemInfo(item);
        }

        private async Task<CheckListItemInfo[]> CreateCheckListItemRangeAsync(int checklistId, CheckListItemCreate[] items)
        {
            var clitems = new List<CheckListItem>();

            foreach (var i in items)
            {
                clitems.Add(new CheckListItem
                {
                    Name = i.Name,
                    Importance = i.Importance,
                    CheckListId = checklistId
                });
            }

            await _context.CheckListItems.AddRangeAsync(clitems);
            await _context.SaveChangesAsync();

            return clitems.Select(e => new CheckListItemInfo(e)).ToArray();
        }

        public async Task<CheckListItemInfo> ChangeCheckListItemAsync(CheckListItemInfo item)
        {
            var clitem = await _context.CheckListItems.Where(s => s.Id == item.Id).FirstOrDefaultAsync();

            if (clitem == null)
                throw new DataServiceException(ApiError.Common.NotFound);

            if (item.Name != null)
                clitem.Name = item.Name;

            if (item.Importance != null)
                clitem.Importance = item.Importance.Value;

            await _context.CheckListItems.AddAsync(clitem);
            await _context.SaveChangesAsync();

            return new CheckListItemInfo(clitem);
        }

        public async Task DeleteCheckListItemAsync(int id)
        {
            throw new NotImplementedException();
        }
    }
}
