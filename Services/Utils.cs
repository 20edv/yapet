﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tonakai.Services
{
    public static class Utils
    {
        public static void AddDataServices(this IServiceCollection services)
        {
            services.AddScoped<UserDataService>();
            services.AddScoped<ProjectDataService>();
            services.AddScoped<EquipmentDataService>();
            services.AddScoped<SectionDataService>();
            services.AddScoped<TitleDataService>();
            services.AddScoped<CheckListDataService>();
            services.AddScoped<FileDataService>();
            services.AddScoped<NoteDataService>();
        }
    }
}
