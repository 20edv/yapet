﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tonakai.Contexts;
using Tonakai.Models.API;
using Tonakai.Models.API.Titles;
using Tonakai.Models.DB;

namespace Tonakai.Services
{
    public class TitleDataService
    {
        private readonly AppDbContext _context;

        public TitleDataService(AppDbContext context)
        {
            _context = context;
        }

        public async Task<TitleInfo[]> GetTitlesAsync(int project)
        {
            return await _context.TitleItems.Where(t => t.ProjectId == project).Select(t => new TitleInfo(t)).ToArrayAsync();
        }

        public async Task<TitleInfo[]> GetTitlesAsync(int project, int count, int offset = 0)
        {
            return await _context.TitleItems.Where(t => t.ProjectId == project).Skip(offset).Take(count).Select(t => new TitleInfo(t)).ToArrayAsync();
        }

        public async Task<TitleInfo> GetTitleAsync(int id)
        {
            var proj = await _context.TitleItems.Where(t => t.Id == id).Select(p => new TitleInfo(p)).FirstOrDefaultAsync();

            if (proj == null)
                throw new DataServiceException(ApiError.Common.NotFound);

            return proj;
        }
        
        public async Task<TitleInfo> CreateTitleAsync(TitleCreate title)
        {
            var titleItem = new TitleItem
            {
                Name = title.Name,
                Description = title.Desc,
                ProjectId = title.Project
            };

            await _context.TitleItems.AddAsync(titleItem);
            await _context.SaveChangesAsync();

            return new TitleInfo(titleItem);
        }

        public async Task<TitleInfo> ChangeTitleAsync(TitleInfo title)
        {
            var titleItem = await _context.TitleItems.FirstOrDefaultAsync(p => p.Id == title.Id);

            if (titleItem == null)
                throw new DataServiceException(ApiError.Common.NotFound);

            if (title.Name != null)
                titleItem.Name = title.Name;

            if (title.Desc != null)
                titleItem.Description = title.Desc;

            await _context.SaveChangesAsync();

            return new TitleInfo(titleItem);
        }

        public async Task<ActionResult> DeleteTitleAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<ActionResult> RecoverTitleAsync(int id)
        {
            throw new NotImplementedException();
        }
    }
}
