﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tonakai.Contexts;
using Tonakai.Models.API;
using Tonakai.Models.API.User;
using Tonakai.Models.DB;

namespace Tonakai.Services
{
    public class UserDataService
    {
        private readonly AppDbContext _context;
        private readonly UserManager<User> _userManager;

        public UserDataService(AppDbContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<UserInfo> CreateUserAsync(UserCreate user)
        {
            var usr = new User
            {
                Email = user.Email,
                UserName = user.Login,
                FirstName = user.FirstName,
                LastName = user.LastName,
                MidName = string.IsNullOrWhiteSpace(user.MidName) ? null : user.MidName,
                Department = string.IsNullOrWhiteSpace(user.Department) ? null : user.Department
            };
            var result = await _userManager.CreateAsync(usr, user.Password);
            if (result.Succeeded)
                return new UserInfo(usr);
            else
                throw new DataServiceException(result.Errors.Select(e => e.Code).ToArray());
        }

        public async Task<UserInfo[]> GetUsersAsync()
        {
            return await _context.Users.Select(u => new UserInfo(u)).ToArrayAsync();
        }

        public async Task<UserInfo[]> GetUsersAsync(int count, int offset = 0)
        {
            return await _context.Users.Skip(offset).Take(count).Select(u => new UserInfo(u)).ToArrayAsync();
        }

        public async Task<UserInfo> GetUserAsync(string id)
        {
            var user = await _context.Users.Where(u => u.Id == id).Select(u => new UserInfo(u)).FirstOrDefaultAsync();

            if (user == null)
                throw new DataServiceException(ApiError.Common.NotFound);

            return user;
        }

        public async Task<UserInfo> ChangeUserAsync(UserInfo user)
        {
            var usr = await _context.Users.FirstOrDefaultAsync(u => u.Id == user.Id);

            if (user == null)
                throw new DataServiceException(ApiError.Common.NotFound);

            if (user.Email != null && user.Email.ToUpper() == usr.NormalizedEmail)
            {
                var result = await _userManager.SetEmailAsync(usr, user.Email);
                if (!result.Succeeded)
                    throw new DataServiceException(result.Errors.Select(e => e.Code).ToArray());
            }

            if (user.FirstName != null)
                usr.FirstName = user.FirstName;

            if (user.LastName != null)
                usr.LastName = user.LastName;

            if (user.MidName != null)
                usr.MidName = string.IsNullOrWhiteSpace(user.MidName) ? null : user.MidName;

            if (user.Department != null)
                usr.Department = string.IsNullOrWhiteSpace(user.Department) ? null : user.Department;

            await _context.SaveChangesAsync();

            return new UserInfo(usr);
        }

        public async Task ChangeUserPasswordAsync(string id, string password)
        {
            throw new NotImplementedException();
            //var user = await _context.Users.FirstOrDefaultAsync(u => u.Id == id);

            //if (user == null)
            //    throw new DataServiceException(ApiError.Common.NotFound);

            //_userManager.RemovePasswordAsync(user);
        }
    }
}
