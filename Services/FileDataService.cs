﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tonakai.Contexts;
using Tonakai.Models.API;
using Tonakai.Models.API.File;
using Tonakai.Models.DB;

namespace Tonakai.Services
{
    public class FileDataService
    {
        private readonly AppDbContext _context;

        public FileDataService(AppDbContext context)
        {
            _context = context;
        }

        public async Task<(byte[] Data, FileInfo Info)> GetFile(string id)
        {
            var file = _context.Files.Where(f => f.Id == id).FirstOrDefault();

            if (file == null)
                throw new DataServiceException(ApiError.Common.NotFound);

            return (file.Data, new FileInfo(file));
        }

        public async Task<FileInfo> GetFileInfo(string id)
        {
            var file = _context.Files.Where(f => f.Id == id).Select(f => new FileInfo(f)).FirstOrDefault();

            if (file == null)
                throw new DataServiceException(ApiError.Common.NotFound);

            return file;
        }

        public async Task<FileInfo> CreateFileAsync(FileCreate file)
        {
            var img = file.Data.Split(";");
            var data = Convert.FromBase64String(img[1].Substring(7));

            var md5 = System.Security.Cryptography.MD5.Create();
            byte[] hash = md5.ComputeHash(data);

            File any = await _context.Files.FirstOrDefaultAsync(f => f.Hash == hash);
            if (any != null)
                return new FileInfo(any);

            var f = new File
            {
                Name = file.Name,
                Data = data,
                Size = data.Length,
                MimeType = img[0].Substring(5),
                Hash = hash,
                UploadDate = DateTime.Now
            };
            _context.Files.Add(f);
            await _context.SaveChangesAsync();
            return new FileInfo(f);
        }
    }
}
