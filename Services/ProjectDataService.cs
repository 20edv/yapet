﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tonakai.Contexts;
using Tonakai.Models.API;
using Tonakai.Models.API.Project;
using Tonakai.Models.DB;

namespace Tonakai.Services
{
    public class ProjectDataService
    {
        private readonly AppDbContext _context;

        public ProjectDataService(AppDbContext context)
        {
            _context = context;
        }

        public async Task<ProjectInfo[]> GetProjectsAsync()
        {
            return await _context.Projects.Select(p => new ProjectInfo(p)).ToArrayAsync();
        }

        public async Task<ProjectInfo[]> GetProjectsAsync(int count, int offset = 0)
        {
            return await _context.Projects.Skip(offset).Take(count).Select(p => new ProjectInfo(p)).ToArrayAsync();
        }

        public async Task<ProjectInfo> GetProjectAsync(int id)
        {
            var proj = await _context.Projects.Where(p => p.Id == id).Select(p => new ProjectInfo(p)).FirstOrDefaultAsync();

            if (proj == null)
                throw new DataServiceException(ApiError.Common.NotFound);

            return proj;
        }

        public async Task<ProjectInfo> CreateProjectAsync(ProjectCreate project)
        {
            var proj = new Project
            {
                Name = project.Name
            };

            await _context.Projects.AddAsync(proj);
            await _context.SaveChangesAsync();

            return new ProjectInfo(proj);
        }

        public async Task<ProjectInfo> ChangeProjectAsync(ProjectInfo project)
        {
            var proj = await _context.Projects.FirstOrDefaultAsync(p => p.Id == project.Id);

            if (proj == null)
                throw new DataServiceException(ApiError.Common.NotFound);

            if (project.Name != null)
                proj.Name = project.Name;

            await _context.SaveChangesAsync();

            return new ProjectInfo(proj);
        }

        public async Task<ProjectStats> GetProjectStatsAsync(int projectId)
        {
            var proj = await _context.Projects.FirstOrDefaultAsync(p => p.Id == projectId);

            if (proj == null)
                throw new DataServiceException(ApiError.Common.NotFound);

            var users = (await _context.CheckNotes.Where(n => n.ProjectId == projectId).Select(n => n.CreatedById).Distinct().ToListAsync()).Concat(
                    await _context.CheckNotes.Where(n => n.ProjectId == projectId && n.AnsweredById != null).Select(n => n.AnsweredById).Distinct().ToListAsync()
                ).Distinct().ToArray();

            var stats = new ProjectStats
            {
                Name = proj.Name,

                Users = await _context.Users.Where(u => users.Contains(u.Id)).Select(u => new ProjectStats.UserStats
                {
                    User = new Models.API.User.UserInfo(u),
                    Created = _context.CheckNotes.Count(n => n.ProjectId == projectId && n.CreatedById == u.Id && n.Status != CheckNote.StatusEnum.Deleted),
                    Answered = _context.CheckNotes.Count(n => n.ProjectId == projectId && n.AnsweredById == u.Id && n.Status != CheckNote.StatusEnum.Deleted)
                }).ToArrayAsync()
            };
            stats.Notes = new ProjectStats.NotesStats
            {
                Actual = await _context.CheckNotes.CountAsync(n => n.ProjectId == projectId && n.Status == CheckNote.StatusEnum.Actual),
                ReqCheck = await _context.CheckNotes.CountAsync(n => n.ProjectId == projectId && n.Status == CheckNote.StatusEnum.ReqCheck),
                Closed = await _context.CheckNotes.CountAsync(n => n.ProjectId == projectId && n.Status == CheckNote.StatusEnum.Closed)
            };
            return stats;
        }
    }
}
