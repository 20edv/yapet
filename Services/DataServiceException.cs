﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tonakai.Models.API;

namespace Tonakai.Services
{
    public class DataServiceException : Exception
    {
        public DataServiceException(params string[] errors)
        {
            ErrorCodes = errors;
        }

        public string[] ErrorCodes { get; set; }

        public ApiError ApiError { get => new ApiError(ErrorCodes); }

        public override string Message => $"Errors: {ErrorCodes}";
    }
}
